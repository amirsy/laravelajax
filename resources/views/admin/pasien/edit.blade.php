@extends('admin.templates.default')

@section('content')

    <div class="box">
        <div class="box-header">
            <h2 class="box-title">UPDATE DATA PASIEN</h2>
        </div>

            <div class="box-body">
                <form action="{{ route('admin.pasien.update', $pasien->pasien_id) }}"
                method="POST" enctype="multipart/form-data">
                @csrf
                @method("PUT")

                {{-- <div class="form-group @error('author') has-error @enderror">
                  <label for="">JENIS PASIEN</label>

                  <select name="jenispasien_id" id="" class="form-control select2">
                      @foreach ($jenispasien as $js)
                              <option value="{{ $jenispasien->id}}"
                                  @if ($jenispasien->id === $pasien->jenispasien_id)
                                          selected
                                      @endif
                                  >
                                  {{ $jenispasien->nama_jenis_pasien}}
                              </option>
                      @endforeach
                  </select>
                  @error('pasein')
                      <span class="help-block">{{ $message}}</span>
                  @enderror
              </div> --}}

                {{-- <div class="form-group @error('jenispasien') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">JENIS PASIEN</label>
                  <div class="col-sm-10">
                    <select name="jenispasien_id" id="" class="form-control select2">
                      @foreach ($jenispasien as $ps)
                             <option value="{{ $ps->jenispasien_id}}">{{ $ps->nama_jenis_pasien}}</option>
                      @endforeach
                         @error('jenispasien')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}

                {{-- <div class="form-group @error('poliklinik') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">POLI KLINIK</label>
                  <div class="col-sm-10">        
                    <select name="poliklinik_id" id="" class="form-control select2">
                      @foreach ($poliklinik as $ps)
                             <option value="{{ $ps->poliklinik_id}}">{{ $ps->nama_poliklinik}}</option>
                      @endforeach
                         @error('poliklinik')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}

                {{-- <div class="form-group @error('agama') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">AGAMA</label>
                  <div class="col-sm-10">
                    <select name="agama_id" id="" class="form-control select2">
                      @foreach ($agama as $ps)
                             <option value="{{ $ps->agama_id}}">{{ $ps->nama_agama}}</option>
                      @endforeach
                         @error('agama')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}

                {{-- <div class="form-group @error('golongan_darah') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">GOLONGAN DARAH</label>
                  <div class="col-sm-10">
                    <select name="golongan_darah_id" id="" class="form-control select2">
                      @foreach ($golongan_darah as $ps)
                             <option value="{{ $ps->golongan_darah_id}}">{{ $ps->nama_golongan_darah}}</option>
                      @endforeach
                         @error('golongan_darah')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}

                {{-- <div class="form-group @error('pekerjaan') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">PEKERJAAN</label>
                  <div class="col-sm-10">
                    <select name="pekerjaan_id" id="" class="form-control select2">
                      @foreach ($pekerjaan as $ps)
                             <option value="{{ $ps->pekerjaan_id}}">{{ $ps->nama_pekerjaan}}</option>
                      @endforeach
                         @error('pekerjaan')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}

                {{-- <div class="form-group @error('pendidikan') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">PENDIDIKAN</label>
                  <div class="col-sm-10">
                    <select name="pendidikan_id" id="" class="form-control select2">
                      @foreach ($pendidikan as $ps)
                             <option value="{{ $ps->pendidikan_id}}">{{ $ps->nama_pendidikan}}</option>
                      @endforeach
                         @error('pendidikan')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}

                {{-- <div class="form-group @error('status_perkawinan') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">STATUS KAWIN</label>
                  <div class="col-sm-10">
                    <select name="status_perkawinan_id" id="" class="form-control select2">
                      @foreach ($status_perkawinan as $ps)
                             <option value="{{ $ps->status_perkawinan_id}}">{{ $ps->nama_status_perkawinan}}</option>
                      @endforeach
                         @error('status_perkawinan')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}

                {{-- <div class="form-group @error('suku') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">SUKU</label>
                  <div class="col-sm-10">
                    <select name="suku_id" id="" class="form-control select2">
                      @foreach ($suku as $ps)
                             <option value="{{ $ps->suku_id}}">{{ $ps->nama_suku}}</option>
                      @endforeach
                         @error('suku')
                             <span class="help-block">{{ $message}}</span>
                         @enderror
                     </select>
                  </div>
                </div> --}}


                <div class="form-group @error('no_rekam_medik') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">NO RM</label>
                  <div class="col-sm-10">
                    <input type="text" name="no_rekam_medik" value="{{ old('no_rekam_medik') }}" class="form-control" id="inputPassword3" placeholder="">
                      @error('no_rekam_medik')
                          <span class="help-block">{{ $message}}</span>
                      @enderror
                  </div>
                </div>

                <div class="form-group @error('nama_pasien') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">NAMA </label>
                  <div class="col-sm-10">
                    <input type="text" name="nama_pasien" value="{{ old('nama_pasien') }}" class="form-control" id="inputPassword3" placeholder="">
                      @error('nama_pasien')
                          <span class="help-block">{{ $message}}</span>
                      @enderror
                  </div>
                </div>

                <div class="form-group @error('tanggal_lahir') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">TANGGAL LAHIR</label>
                  <div class="col-sm-10">
                    <input type="date" name="tanggal_lahir" value="{{ old('tanggal_lahir') }}" class="form-control" id="inputPassword3" placeholder="">
                    @error('tanggal_lahir')
                      <span class="help-block">{{ $message}}</span>
                    @enderror
                  </div>
                </div>

                <div class="form-group @error('cover') has-error @enderror">
                  <label for="inputPassword3" class="col-sm-2 control-label">COVER</label>
                  <div class="col-sm-10">
                    <input type="file" name="cover"  class="form-control" id="inputPassword3" placeholder="">
                    @error('cover')
                      <span class="help-block">{{ $message}}</span>
                    @enderror
                  </div>
                </div>

                  
                    <div class="form-group">
                        <input type="hidden" name="pasien" value="{{$pasien->pasien_id}}">
                        <input type="submit" value="Update" class ="btn btn-primary">
                        <button type="reset" class="btn btn-warning">Ulang</button>
                        <a href="{{ route('admin.pasien.index') }}" class="btn btn-danger">Kembali</a>
                    </div>

                </form>
            </div>
    </div>
@endsection


