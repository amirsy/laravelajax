@extends('admin.templates.default')
@section('content')

<!-- /.box-header -->
    <div class="box">
        <div class="box-header">
                <h3 class="box-title">DATA COMPANY</h3><br><br>
                {{-- <a href="{{ route('admin.company.create') }}" class="btn btn-primary">TAMBAH DATA COMPANY</a> --}}

                <a class="btn btn-success" href="javascript:void(0)" id="createNewCompany">Tambah Data Company</a>
                <a href="" class="btn btn-primary">Export Excel</a> 
                <a href="" class="btn btn-primary">Export PDF</a>  
            </div>
            <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width='1'>ID</th>
                            <th width='10'>NAMA COMPANY</th>
                            <th width='10'>NAMA KARYAWAN</th>
                            <th width='10'>NAMA CUTI</th>
                            <th width='5'>TANGGAL GABUNG KARYAWAN</th>
                            <th width='5'>LAMA CUTI KARYAWAN</th>
                            <th width='1'>HARGA</th>
                            <th width='1'>EMAIL</th>
                            <th width='10'>COVER</th>
                            <th width='5'>KETERANGAN</th>
                            <th width='5'>ALAMAT</th>
                            <th> </th>
                        </tr>
                    </thead>
                    {{-- <tbody>
                    </tbody> --}}
            </table>
        </div>
    </div>

{{-- create ajax --}}
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="companyForm" name="companyForm" class="form-horizontal">

                   <input type="hidden" name="company_id" id="company_id">
                   
                   <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">NAMA KOTA</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nama_company" name="nama_company" 
                        placeholder="Enter Nama Company" value="" maxlength="50" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">NAMA KARYAWAN</label>
                    <div class="col-sm-10">
                      <select name="karyawan_id" id="" class="form-control select2">
                        {{-- @foreach ($karyawan as $p)
                               <option value="{{ $p->karyawan_id}}">{{ $p->nama_karyawan}}</option>
                        @endforeach                  --}}
                       </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">NAMA CUTI</label>
                    <div class="col-sm-10">
                      <select name="cuti_id" id="" class="form-control select2">
                        {{-- @foreach ($provinsi as $p)
                               <option value="{{ $p->cuti_id}}">{{ $p->nama_cuti}}</option>
                        @endforeach                  --}}
                       </select>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">TANGGAL GABUNG KARYAWAN</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" id="tanggal_gabung_karyawan_id" name="tanggal_gabung_karyawan" 
                        placeholder="" value=""  required>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">LAMA CUTI KARYAWAN</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="lama_cuti_karyawan_id" name="lama_cuti_karyawan" 
                        placeholder=" Nama Cuti Karyawan " value="" maxlength="50" required>
                    </div>
                </div> 

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">HARGA</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="harga_id" name="harga" 
                        placeholder="Enter Nama Kota" value="" maxlength="50" required>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">EMAIL</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email_id" name="email" 
                        placeholder="Enter Nama Email" value="" maxlength="50" required>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">COVER 1</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" id="kode_id" name="cover" 
                        placeholder="Enter Nama Kota" value="" maxlength="50" required>
                    </div>
                </div>  
                {{-- <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">COVER 2</label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" id="kode_id" name="kode_pos" 
                        placeholder="Enter Nama Kota" value="" maxlength="50" required>
                    </div>
                </div>                             --}}
                <div class="form-group">
                    <label class="col-sm-2 control-label">KETERANGAN</label>
                    <div class="col-sm-10">
                        <textarea id="keterangan-edit" name="keterangan" required
                        placeholder="Enter Keterangan" class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">ALAMAT</label>
                    <div class="col-sm-10">
                        <textarea id="alamat-edit" name="alamat" required
                        placeholder="Enter Alamat" class="form-control"></textarea>
                    </div>
                </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Simpan</button>
                            <button type="submit" class="btn btn-info" id="" value="">Kembali</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- editajax  --}}
<div class="modal fade" id="ajaxModelEdit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="updatekotaForm" name="kotaForm" class="form-horizontal">

                   <input type="hidden" name="kota_id" id="kota_id">
                   
                   <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">NAMA KOTA</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_company" name="nama_company" 
                            placeholder="Enter Nama Company" value="" maxlength="50" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">NAMA KARYAWAN</label>
                        <div class="col-sm-10">
                          <select name="karyawan_id" id="" class="form-control select2">
                            {{-- @foreach ($karyawan as $p)
                                   <option value="{{ $p->karyawan_id}}">{{ $p->nama_karyawan}}</option>
                            @endforeach                  --}}
                           </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">NAMA CUTI</label>
                        <div class="col-sm-10">
                          <select name="cuti_id" id="" class="form-control select2">
                            {{-- @foreach ($provinsi as $p)
                                   <option value="{{ $p->cuti_id}}">{{ $p->nama_cuti}}</option>
                            @endforeach                  --}}
                           </select>
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">TANGGAL GABUNG KARYAWAN</label>
                        <div class="col-sm-10">
                            <input type="date" class="form-control" id="tanggal_gabung_karyawan_id" name="tanggal_gabung_karyawan" 
                            placeholder="" value=""  required>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">LAMA CUTI KARYAWAN</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="lama_cuti_karyawan_id" name="lama_cuti_karyawan" 
                            placeholder=" Nama Cuti Karyawan " value="" maxlength="50" required>
                        </div>
                    </div> 

                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">HARGA</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="harga_id" name="harga" 
                            placeholder="Enter Nama Kota" value="" maxlength="50" required>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">EMAIL</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_id" name="email" 
                            placeholder="Enter Nama Email" value="" maxlength="50" required>
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">COVER 1</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="kode_id" name="cover" 
                            placeholder="Enter Nama Kota" value="" maxlength="50" required>
                        </div>
                    </div>  
                    {{-- <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">COVER 2</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="kode_id" name="kode_pos" 
                            placeholder="Enter Nama Kota" value="" maxlength="50" required>
                        </div>
                    </div>                             --}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">KETERANGAN</label>
                        <div class="col-sm-10">
                            <textarea id="keterangan-edit" name="keterangan" required
                            placeholder="Enter Keterangan" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">ALAMAT</label>
                        <div class="col-sm-10">
                            <textarea id="alamat-edit" name="alamat" required
                            placeholder="Enter Alamat" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="" id="kota_id_edit">
                            <button type="submit" class="btn btn-primary"value="create">Simpan</button>
                            <button  class="btn btn-info" id="" value="">Kembali</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('scripts')
    {{-- <script>
        $(function () {
            $('#dataTable').DataTable({
            });
        });
    </script> --}}
      <script type="text/javascript">
        $(function (){
    
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                    });
                        // Index
                        var table = $('#dataTable').DataTable({
                            "pageLength": 50,
                            processing: true,
                            serverSide: true,
                            ajax: '{{ route('admin.company.index') }}',
                            columns: [
                                { data: 'DT_RowIndex', orderable: false, searchable : false}, 
                                // {data: 'provinsi_id', name: 'provinsi_id'},
                                {data: 'nama_company, name: 'nama_company'},
                                {data: 'nama_karyawan, name: 'nama_karyawan'},
                                {data: 'nama_cuti, name: 'nama_cuti'},
                                {data: 'tanggal_gabung_karyawan', name: 'tanggal_gabung_karyawan'},
                                {data: 'lama_cuti_karyawan', name: 'lama_cuti_karyawan'},
                                {data: 'harga', name: 'harga'},
                                {data: 'email', name: 'email'},
                                {data: 'cover', name: 'cover'},
                                {data: 'keterangan', name: 'keterangan'},
                                {data: 'alamat', name: 'alamat'},
                                {data: 'action', name: 'action', orderable: false, searchable: false},
                            ]
                        }); 

                        //create 
                        $('#createNewCompany').click(function () {
                            $('#saveBtn').val("create-company");
                            $('#company_id').val("");
                            $('#companyForm').trigger("reset");
                            $('#modelHeading').html("Tambah Data Company");
                            $('#ajaxModel').modal("show");
                        });

             
                        //createupdate
                        // $('#saveBtn').click(function (e) {
                        // e.preventDefault();
                        // $(this).html('Save');
                        //     $.ajax({
                        //         data: $('#kotaForm').serialize(),
                        //         url: "{{ route('admin.kota.store') }}",
                        //         type: "POST",
                        //         dataType: 'json',
                        //         success: function (data) {
                        //             $('#kotaForm').trigger("reset");
                        //             $('#ajaxModel').modal('hide');
                        //             table.draw();
                        //         },
                        //         error: function (data) {
                        //             console.log('Error:', data);
                        //             $('#saveBtn').html('Save Changes');
                        //         }
                        //     });
                        // });
 
                       //edit
                        // $('body').on('click', '.edit', function () {
                        //     $('#kota').val($(this).data('title'));
                        //     $('#provinsi').val($(this).data('provinsi'));
                        //     $('#kode_id').val($(this).data('kode_pos'));
                        //     $('#keterangan-edit').val($(this).data('keterangan'));
                        //     $('#kota_id_edit').val($(this).data('id'));
                        //     $('#modHeading').html("Edit Data Kota");
                        //     $('#ajaxModelEdit').modal('show');
                        //     return false;
                        // });

                          //updateKotaForm
                        //   $('#updatekotaForm').submit(function (e) {
                        //     e.preventDefault();
                        //     //$(this).html('Save');
                        //         $.ajax({
                        //             data: $('#updatekotaForm').serialize(),
                        //             url: "{{ route('admin.kota.update') }}",
                        //             type: "POST",
                        //             dataType: 'json',
                        //             success: function (data) {
                        //                 $('#ajaxModelEdit').modal('hide');
                        //                 // $('#dataTable').DataTable().fnDestroy();
                        //                 //table.ajax.reload();
                        //                 table.draw();
                        //             },
                        //             error: function (data) {
                        //                 console.log('Error:', data);
                        //                 $('#updateprovinsiForm').html('Save Changes');
                        //             }
                        //         });
                        // });

                        //delete
                        // $('body').on('click', '.deleteKota', function () {
                        //     var kota_id = $(this).data("kota_id");
                        //     confirm("Yakin data ingin di hapus !!!");
                        //         $.ajax({
                        //             type: "DELETE",
                        //             url: "{{ route('admin.kota.destroy') }}",
                        //             data: {id:kota_id,_method:'delete'},
                        //             function (data) {
                        //                 //table.draw();
                        //                 //console.log('kene');

                        //                 $('#dataTable').DataTable().fnDestroy();
                        //                 datatable();
                        //             },
                        //             error: function (data) {
                        //                 console.log('Error:', data);
                        //             }
                        //         });
                        //         table.draw();
                        //     });                                      
            }); 
    </script> 
@endpush



{{-- @push('scripts')
<script>
    $(function () {
        $('#dataTable').DataTable({

    });
</script>
@endpush --}}





 {{-- <script>
//     $(function () {
//         $('#dataTable').DataTable({
//             processing: true,
//             serverSide: true,
//             ajax: '{{ route('admin.cuti.data') }}',
//             columns: [
//                 //{ data: 'id'},
//                 { data: 'DT_RowIndex', orderable: false, searchable : false}, 
//                 { data: 'nama'},
//                 { data: 'keterangan'},
//                 { data: 'tanggal_cuti'},
//                 { data: 'tanggal_selesai_cuti'},
//                 { data: 'lama_cuti'},
//                 { data: 'sisa_cuti'},
//                 { data: 'action'},
//             ]
//         });
//     });
// </script> --}}


{{-- @foreach ($cuti as $c )
<tr>
    <td width='5'>  {{ $loop-> index +1 }} </td>
    <td width='60'><img class="img-responsive" src="{{url('/assets/covers/'. $c->cover)}}"> </td>
    <td width='20'> {{ $c->nama_cuti  }}  </td>
    <td width='20'> {{ $c->nama  }}  </td>
    <td width='20'> {{ $c->keterangan  }}  </td>
    <td width='20'> {{ $c->tanggal_cuti  }}  </td>
    <td width='20'> {{ $c->tanggal_selesai_cuti  }}  </td>
    <td width='20'> {{ $c->lama_cuti  }}  </td>
    <td width='20'> {{ $c->sisa_cuti  }}  </td>
    <td width='20'> {{ $c->email  }}  </td>
    <td width='5'>  <a href="" class="btn btn-info">DETAIL</a></td>
    <td width='5'>  <a href="" class="btn btn-warning">EDIT</a></td>
    <td width='5'>
        <form action="{{ route('admin.cuti.destroy', $c->cuti_id) }}" method="post" style="display:inline;">
            {{ csrf_field() }}
            {{ method_field ('delete')}}
            <button type="submit"  class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')">DELETE</button>
        </form>
    </td>
</tr>
@endforeach --}}