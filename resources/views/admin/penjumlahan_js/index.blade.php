@extends('admin.templates.default')
@section('content')
<h1> DATA PENJUMLAHAN JAVA SCRIPT</h1>

<!-- /.box-header -->
    <div class="box">
        <div class="box-header">
            <center> <h3 class="box-title" style="text-transform: uppercase;">Silahkan input data </h3><br><br> </center>
                <form action="" class="form-horizontal" method="POST" enctype="multipart/form-data" style="max-width:500px">
                    @csrf
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">BILANGAN SATU</label>
                            <div class="col-sm-10">
                                <input type="text" id="bil_satu" name="bil_satu" class="form-control" placeholder="Input bil satu">
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">BILANGAN DUA</label>
                            <div class="col-sm-10">
                                <input type="text" name="bil_dua" class="form-control" id="bil_dua" placeholder="Bil Dua">
                            </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <center>
                            <tr>
                                {{-- <td><input type="submit" value="Tambah" class="btn btn-primary"></td>
                                <td><input type="reset" name="batal" class="btn btn-warning" value="RESET"></td> --}}
                                <button onclick="ftambah()">Tambah (+) </button>
                                <button>Kurang (-)</button>    
                            </tr>
                        </center>
                        </div>
                    </div>
                </form>
            </div>
            <div class="box-body">

            <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width='5'>ID</th>
                            <th width='5'>BILANGAN SATU</th>
                            <th width='5'>BILANGAN DUA</th>
                            <th width='5'>JUMLAH</th>
                            <th width='5'>FUNGSI TERBILANG</th>
                            <th width='5'>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                    
                    </tbody>
            </table>
        </div>
    </div>
@endsection

<script>
    var bil_satu = document.getElementById("bil_satu");
    var bil_dua  = document.getElementById("bil_dua");
    var jumlah   = document.getElementById("jumlah");

    function ftambah() {
        jumlah.value = Number(bil_satu.value) + Number(bil_dua.value);
    }

    function fkurang() {
        jumlah.value = Number(bil_satu.value)  Number(bil_dua.value);
    }
</script>
@push('scripts')
    <script>
        $(function () {
            $('#dataTable').DataTable({


            });
        });
    </script>
@endpush

