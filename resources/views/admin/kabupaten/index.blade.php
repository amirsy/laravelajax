@extends('admin.templates.default')
@section('content')
<h1>KABUPATEN</h1>

 <!-- /.box-header -->
 <div class="box">
        <div class="box-header">
              <h3 class="box-title">DATA KABUPATEN</h3><br><br>
              
              <a class="btn btn-success" href="javascript:void(0)" id="createNewKabupaten">Tambah Data Kabupaten</a>
              <a href="" class="btn btn-primary">Export Excel</a> 
              <a href="" class="btn btn-primary">Export PDF</a>   
         </div>
         <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAMA KABUPATEN</th>
                            <th>NAMA KOTA</th>
                            <th>NAMA PROVINSI</th>
                            <th>CODE POS</th>
                            <th>EMAIL</th>
                            <th>COVER</th>
                            <th>KETERANGAN</th>
                            <th width="130px">ACTION</th>
                        </tr>
                    </thead>
                    {{-- <tbody>
                    </tbody> --}}
            </table>
        </div>
</div>

{{-- create ajax --}}
<div class="modal fade" id="ajaxModel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modelHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="kabupatenForm" name="kabupatenForm" class="form-horizontal">

                   <input type="hidden" name="kabupaten_id" id="kabupaten_id">
                   
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">NAMA KABUPATEN</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nama_kabupaten" name="nama_kabupaten" 
                            placeholder="Enter Nama Kabupaten" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">NAMA PROVINSI</label>
                        <div class="col-sm-8">
                          <select name="provinsi_id" id="provinsi_id" class="form-control select2">
                              {{-- <option value="0">Pilih Provinsi</option> --}}
                            @foreach ($provinsi as $p)
                                   <option value="{{ $p->provinsi_id}}">{{ $p->nama_provinsi}}</option>
                            @endforeach
                               @error('jenispasien')
                                   <span class="help-block">{{ $message}}</span>
                               @enderror
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">NAMA KOTA</label>
                        <div class="col-sm-8">
                          <select name="kota" id="kotaadd" class="form-control select2">
                           </select>
                        </div>
                    </div>
                
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">EMAIL</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_id" name="email" 
                            placeholder="Enter Email" value="" maxlength="50" required >
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">KODE POS</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="kode_pod" name="kode_pos" 
                            placeholder="Enter Nama Kota" value="" maxlength="50" required="">
                        </div>
                    </div>  --}}
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">KODE POS</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode_pos" name="kode_pos" 
                            placeholder="Enter Kode Pos" value="" maxlength="50" required >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">FOTO</label>
                        <div class="col-sm-5">
                            <input type="file" class="form-control" id="" name="image" 
                            placeholder="" value="" maxlength="50" required >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">KETERANGAN</label>
                        <div class="col-sm-10">
                            <textarea id="keterangan" name="keterangan" required 
                            placeholder="Enter Deskripsi" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Simpan</button>
                            <button type="submit" class="btn btn-info" id="" value="">Kembali</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- editajax  --}}
<div class="modal fade" id="ajaxModelEdit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modHeading"></h4>
            </div>
            <div class="modal-body">
                <form id="updatekabupatenForm" name="kabupatenForm" class="form-horizontal">

                   <input type="hidden" name="kabupaten_id" id="kabupaten_id">
                   
                   <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">NAMA KABUPATEN</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kabupaten" name="nama_kabupaten" 
                            placeholder="Enter Nama Kabupaten" value="" maxlength="50" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">NAMA PROVINSI</label>
                        <div class="col-sm-8">
                            <select type="text" class="form-control" id="provinsi" name="nama_provinsi" 
                                placeholder="Enter Nama Provinsi" value="" maxlength="50" required >
                                @foreach ($provinsi as $p)
                                        <option value="{{ $p->provinsi_id}}">{{ $p->nama_provinsi}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">NAMA KOTA</label>
                        <div class="col-sm-8">
                          <select name="kota" id="kotaupdate" class="form-control select2">
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">EMAIL</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email_id" name="email" 
                            placeholder="Enter Nama Email" value="" maxlength="50" required >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">KODE POS</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode_id" name="kode_pos" 
                            placeholder="Masukkan Kode Pos" value="" maxlength="50" required="">
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">UPDATE FOTO</label>
                        <div class="col-sm-5">
                            <input type="file" name="image" id="" class="form-control" 
                            placeholder="" value="" required>
                        </div>
                    </div>    
                    <div class="form-group">
                        <label class="col-sm-2 control-label">KETERANGAN</label>
                        <div class="col-sm-10">
                            <textarea id="keterangan-edit" name="keterangan" required 
                            placeholder="Enter Keterangan" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            {{ method_field('PUT') }}
                            <input type="hidden" name="id" value="" id="kabupaten_id_edit">
                            <button type="submit" class="btn btn-primary"value="create">Simpan</button>
                            <button  class="btn btn-info" id="" value="">Kembali</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@push('scripts')

<script src="{{ asset('assets/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    
<script src="{{ asset('/assets/plugins/bs.notify.min.js')}}"></script>
    @include('admin.templates.partials.alerts')
    <!-- //jquery -->

    <script type="text/javascript">
        $(function (){
    
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                    });
                        // Index
                        var table = $('#dataTable').DataTable({
                            "pageLength": 50,
                            processing: true,
                            serverSide: true,
                            ajax: '{{ route('admin.kabupaten.index') }}',
                            columns: [
                                { data: 'DT_RowIndex', orderable: false, searchable : false}, 
                                // {data: 'provinsi_id', name: 'provinsi_id'},
                                {data: 'nama_kabupaten', name: 'nama_kabupaten'},
                                {data: 'nama_kota', name: 'nama_kota'},
                                {data: 'nama_provinsi', name: 'nama_provinsi'},
                                {data: 'kode_pos', name: 'kode_pos'},
                                {data: 'email', name: 'email'},
                                {data: 'image', name: 'image'},
                                {data: 'keterangan', name: 'keterangan'},
                                {data: 'action', name: 'action', orderable: false, searchable: false},
                            ]
                        }); 

                        //create 
                        $('#createNewKabupaten').click(function () {
                            $('#saveBtn').val("create-kabupaten");
                            $('#kabupaten_id').val("");
                            $('#kabupatenForm').trigger("reset");
                            $('#modelHeading').html("Tambah Data Kabupaten");
                            $('#ajaxModel').modal("show");
                        });

                        //createupdate
                        $('#saveBtn').click(function (e) {
                        e.preventDefault();
                        $(this).html('Save');
                            $.ajax({
                                data: $('#kabupatenForm').serialize(),
                                url: "{{ route('admin.kabupaten.store') }}",
                                type: "POST",
                                dataType: 'json',
                                success: function (data) {
                                    $('#kabupatenForm').trigger("reset");
                                    $('#ajaxModel').modal('hide');
                                    table.draw();
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                    $('#saveBtn').html('Save Changes');
                                }
                            });
                        });

                        //edit
                        $('body').on('click', '.edit', function () {
                            $('#kabupaten').val($(this).data('title'));
                            $('#kode_id').val($(this).data('kode_pos'));
                            $('#kota').val($(this).data('kota'));
                            $('#provinsi').val($(this).data('provinsi'));
                            $('#email_id').val($(this).data('email'));
                            $('#keterangan-edit').val($(this).data('keterangan'));
                            $('#kabupaten_id_edit').val($(this).data('id'));
                            $('#modHeading').html("Edit Data Kabupaten");
                            $('#ajaxModelEdit').modal('show');
                            return false;
                        });

                        //editupdateKabupaten
                        $('#updatekabupatenForm').submit(function (e) {
                            e.preventDefault();
                                s.ajax({
                                    data     : $('#updatekabupatenForm').serialize(),
                                    url      : "{{ route('admin.kabupaten.update') }}",
                                    type     : "POST",
                                    dataType : 'json',
                                    success: function (data) {
                                        $('#ajaxModelEdit').modal('hide');
                                        table.draw();
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                        $('#updatekabupatenForm').html('Save Changes');
                                    }
                                });
                        });
      
                        //deletekabupaten_id
                        $('body').on('click', '.deleteKabupaten', function () {
                            var kota_id = $(this).data("kabupaten_id");
                            confirm("Yakin data ingin di hapus !!!");
                                $.ajax({
                                    type: "DELETE",
                                    url: "{{ route('admin.kabupaten.destroy') }}",
                                    data: {id:kabupaten_id,_method:'delete'},
                                    function (data) {
                                        //table.draw();
                                        //console.log('kene');

                                        $('#dataTable').DataTable().fnDestroy();
                                        datatable();
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                                table.draw();
                            });      


                            $("#provinsi_id").change(function() {
                                let provinsiID=$(this).val();
                                $.ajax({
                                    type: "POST",
                                    url: "{{ route('admin.kabupaten.ajax') }}",
                                    
                                    headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                    data: {provinsiID:provinsiID},
                                    success:function (data) {
                                        //table.draw();
                                        //console.log('kene');
                                        console.log(data,'adhajhd');
                                        $('#kotaadd').html(data);
                                        // $('#kotaupdate').html(data);
                                       
                                    },
                                    error: function (data) {
                                        console.log('Error:', data);
                                    }
                                });
                                
                            });


                            
            }); 
        </script>
@endpush

