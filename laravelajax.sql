-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 23 Jul 2022 pada 01.45
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelajax`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agama`
--

CREATE TABLE `agama` (
  `agama_id` int(11) NOT NULL,
  `nama_agama` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `agama`
--

INSERT INTO `agama` (`agama_id`, `nama_agama`, `created_at`, `updated_at`) VALUES
(1, 'ISLAM', '2021-09-22 04:42:11', '0000-00-00 00:00:00'),
(2, 'KHATOLIK', '2021-09-22 04:42:21', '0000-00-00 00:00:00'),
(3, 'KRISTEN', '2021-09-22 04:42:28', '0000-00-00 00:00:00'),
(4, 'KRISTEN ORTODOK', '2021-09-22 04:42:38', '0000-00-00 00:00:00'),
(5, 'HINDU', '2021-09-22 04:42:45', '0000-00-00 00:00:00'),
(6, 'BUHDA', '2021-09-22 04:42:53', '0000-00-00 00:00:00'),
(7, 'KONG HUU CUU', '2021-09-22 04:43:02', '0000-00-00 00:00:00'),
(8, 'KOMUNIS', '2021-09-22 04:43:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bis`
--

CREATE TABLE `bis` (
  `bis_id` int(11) NOT NULL,
  `jumlah` varchar(200) NOT NULL,
  `jenis_bis` varchar(255) NOT NULL,
  `warna` varchar(255) NOT NULL,
  `nama_sopir` varchar(255) NOT NULL,
  `nama_kondektur` varchar(255) NOT NULL,
  `napol` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bis`
--

INSERT INTO `bis` (`bis_id`, `jumlah`, `jenis_bis`, `warna`, `nama_sopir`, `nama_kondektur`, `napol`, `created_at`, `updated_at`) VALUES
(40, '50', 'Big Bus', 'Biru Dongker', 'Arif Babe', 'Ade Oyea', 'AB 789 Bantul', '2021-10-23 08:30:15', NULL),
(42, '3', 'Mini Bus', 'Merah', 'Riki Ayub', 'Febri', 'AB 123 Jogja', '2021-10-23 08:30:24', NULL),
(43, '44', 'Big Bus', 'Biru Dongker', 'Arif Babe', 'Ade Oyea', 'AB 789 Bantul', '2021-10-23 08:30:30', NULL),
(44, '7', 'Mini Bus', 'Merah', 'Riki Ayub', 'Febri', 'AB 123 Jogja', '2021-10-23 08:30:35', NULL),
(50, '12', 'Mini Bus', 'Merah', 'Riki Ayub', 'Febri', 'AB 123 Jogja', '2021-10-23 12:43:49', NULL),
(51, '60', 'Big Bus Level 1', 'Putih Ferari', 'Yoga Jangkrik', 'Kuwe Gatel', 'AB 001 Bantul', '2021-10-29 08:43:42', NULL),
(52, '12', 'Mini Bus', 'Merah', 'Riki Ayub', 'Febri', 'AB 123 Jogja', '2021-11-24 06:15:33', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `company`
--

CREATE TABLE `company` (
  `company_id` bigint(20) UNSIGNED NOT NULL,
  `karyawan_id` bigint(20) UNSIGNED NOT NULL,
  `cuti_id` bigint(20) UNSIGNED NOT NULL,
  `nama_company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_gabung_karyawan` date NOT NULL,
  `lama_cuti_karyawan` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `cuti`
--

CREATE TABLE `cuti` (
  `cuti_id` bigint(20) UNSIGNED NOT NULL,
  `tanggal_cuti` date NOT NULL,
  `nama_cuti` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_selesai_cuti` date NOT NULL,
  `lama_cuti` int(11) NOT NULL,
  `sisa_cuti` int(11) NOT NULL,
  `cover` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cuti`
--

INSERT INTO `cuti` (`cuti_id`, `tanggal_cuti`, `nama_cuti`, `tanggal_selesai_cuti`, `lama_cuti`, `sisa_cuti`, `cover`, `cover2`, `cover3`, `cover4`, `keterangan`, `created_at`, `updated_at`) VALUES
(50, '2022-02-01', 'Cuti Hari Raya', '2022-02-02', 12, 12, '1644071296.png', NULL, NULL, NULL, 'DEDEDEDED', '2022-04-08 00:12:33', '2022-04-08 00:12:33'),
(51, '2022-02-05', 'Cuti NIkah 1 Bulan', '2022-02-10', 10, 12, '1649429373.jpg', NULL, NULL, NULL, 'Nikah Oke', '2022-04-08 14:49:33', '2022-04-08 14:49:33'),
(53, '2022-04-06', 'Cuti ABC', '2022-04-14', 16, 15, '1649377241.png', NULL, NULL, NULL, 'gbgbg', '2022-04-08 00:20:41', '2022-04-08 00:20:41'),
(54, '2022-04-07', 'Cuti Liburan', '2022-04-22', 12, 13, '1649377492.png', NULL, NULL, NULL, 'bgbggbgb', '2022-04-08 00:24:52', '2022-04-08 00:24:52'),
(55, '2022-04-14', 'CUTI BERSAMA', '2022-04-14', 5, 0, '1650496511.jpg', NULL, NULL, NULL, 'CUTI BERSAMA KET', '2022-04-20 23:15:11', '2022-04-20 23:15:11'),
(56, '2022-04-20', 'CUTI HARI NYEPI', '2022-04-20', 5, 0, '1650550355.png', NULL, NULL, NULL, 'KETERANGAN', '2022-04-21 14:12:35', '2022-04-21 14:12:35'),
(57, '2022-04-20', 'CUTI HARI WAISAH', '2022-04-20', 6, 0, '1650550429.png', NULL, NULL, NULL, 'KETERANGAN WAISAH', '2022-04-21 14:13:49', '2022-04-21 14:13:49'),
(58, '2022-04-20', 'HARI RAYA IDUL FITRI', '2022-04-20', 12, 0, '1650550495.png', NULL, NULL, NULL, 'HARI RAYA IDUL FITRI', '2022-04-21 14:14:55', '2022-04-21 14:14:55'),
(59, '2022-04-15', 'CURI 17 AGUSTUS', '2022-04-20', 13, 0, '1650550538.png', NULL, NULL, NULL, 'KETERANGAN', '2022-04-21 14:15:38', '2022-04-21 14:15:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `fungsi_terbilang`
--

CREATE TABLE `fungsi_terbilang` (
  `terbilang_id` bigint(20) UNSIGNED NOT NULL,
  `tanggal_transaksi` bigint(20) UNSIGNED NOT NULL,
  `jam` time NOT NULL,
  `kode_barang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `terbilang` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ganjilgenap`
--

CREATE TABLE `ganjilgenap` (
  `ganjilgenap_id` int(11) NOT NULL,
  `tanggal_mulai` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `napol` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ganjilgenap`
--

INSERT INTO `ganjilgenap` (`ganjilgenap_id`, `tanggal_mulai`, `napol`, `status`, `created_at`, `updated_at`) VALUES
(15, '2021-10-29 13:17:40', 'AB098YP', 'Anda Kena Tilang Rp.1.000.000', '2021-10-29 06:17:40', '2021-10-29 06:17:40'),
(16, '2021-10-29 13:17:50', 'K1237OU', 'Silahkan Lanjut Perjalanan', '2021-10-29 06:17:50', '2021-10-29 06:17:50'),
(19, '2021-10-30 13:02:25', 'AB89800op', 'Silahkan Lanjut Perjalanan Pak Haji', '2021-10-30 06:02:25', '2021-10-30 06:02:25'),
(22, '2021-10-31 07:50:56', 'AB89800op', 'Anda Kena Tilang Rp.1.000.000', '2021-10-31 00:50:56', '2021-10-31 00:50:56'),
(24, '2021-11-24 09:38:58', 'S8768OP', 'Silahkan Lanjut Perjalanan Pak Haji', '2021-11-24 02:38:58', '2021-11-24 02:38:58'),
(26, '2021-11-24 10:03:15', 'L09877YY', 'Anda Kena Tilang Rp.1.000.000', '2021-11-24 03:03:15', '2021-11-24 03:03:15'),
(27, '2021-11-24 10:05:02', 'H00988GG', 'Silahkan Lanjut Perjalanan Pak Haji', '2021-11-24 03:05:02', '2021-11-24 03:05:02'),
(28, '2021-11-24 10:05:15', 'AA7571DD', 'Anda Kena Tilang Rp.1.000.000', '2021-11-24 03:05:15', '2021-11-24 03:05:15'),
(31, '2021-11-24 13:14:03', 'D9866UO', 'Silahkan Lanjut Perjalanan Pak Haji', '2021-11-24 06:14:03', '2021-11-24 06:14:03'),
(32, '2021-11-24 13:14:25', 'D0998YG', 'Silahkan Lanjut Perjalanan Pak Haji', '2021-11-24 06:14:25', '2021-11-24 06:14:25'),
(34, '2022-01-29 15:28:11', 'A9988GT', 'Silahkan Lanjut Perjalanan Pak Haji', '2022-01-29 23:28:11', '2022-01-29 23:28:11'),
(35, '2022-01-30 14:47:18', 'A99999998GT', 'Anda Kena Tilang Rp.1.000.000', '2022-01-30 22:47:18', '2022-01-30 22:47:18'),
(36, '2022-01-30 14:47:39', 'A111GT', 'Silahkan Lanjut Perjalanan Pak Haji', '2022-01-30 22:47:39', '2022-01-30 22:47:39'),
(37, '2022-07-23 06:38:52', 'TT098', 'Anda Kena Tilang Rp.1.000.000', '2022-07-22 23:38:52', '2022-07-22 23:38:52'),
(38, '2022-07-23 06:39:16', 'TT098111', 'Silahkan Lanjut Perjalanan Pak Haji', '2022-07-22 23:39:16', '2022-07-22 23:39:16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `golongan_darah`
--

CREATE TABLE `golongan_darah` (
  `golongan_darah_id` int(11) NOT NULL,
  `nama_golongan_darah` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `golongan_darah`
--

INSERT INTO `golongan_darah` (`golongan_darah_id`, `nama_golongan_darah`, `created_at`, `updated_at`) VALUES
(1, 'A', '2021-09-22 03:22:57', '0000-00-00 00:00:00'),
(2, 'B', '2021-09-22 03:23:03', '0000-00-00 00:00:00'),
(3, 'AB', '2021-09-22 03:23:09', '0000-00-00 00:00:00'),
(4, 'O', '2021-09-22 03:23:15', '0000-00-00 00:00:00'),
(5, 'B+', '2021-09-22 03:23:29', '0000-00-00 00:00:00'),
(6, 'O+', '2021-09-22 03:23:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gol_jam_kerja`
--

CREATE TABLE `gol_jam_kerja` (
  `gol_jam_kerja_id` int(11) NOT NULL,
  `jumlah_jam_kerja` int(11) NOT NULL,
  `tingkatan_kerja` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gol_jam_kerja`
--

INSERT INTO `gol_jam_kerja` (`gol_jam_kerja_id`, `jumlah_jam_kerja`, `tingkatan_kerja`, `created_at`, `updated_at`) VALUES
(1, 10, 'Bagus', '2021-10-24 05:56:17', '2021-10-24 05:56:17'),
(2, 14, '', '2021-10-24 06:08:41', '2021-10-24 06:08:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hari`
--

CREATE TABLE `hari` (
  `hari_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hari`
--

INSERT INTO `hari` (`hari_id`, `nama`, `tanggal_lahir`, `keterangan`, `created_at`, `updated_at`) VALUES
(13, 'Aisah Putri Pratiwi', '2021-11-03', 'Kamu lahir pada hari Minggu ', '2021-11-10 13:10:47', '2021-11-10 13:10:47'),
(14, 'Amir', '1993-09-21', 'Kamu lahir pada hari Rabu ', '2021-11-10 13:11:23', '2021-11-10 13:11:23'),
(15, 'Robby Pratama', '2008-07-17', 'Kamu lahir pada hari Kamis ', '2021-11-10 13:40:54', '2021-11-10 13:40:54'),
(16, 'Sahrukan India', '2021-11-04', 'Kamu lahir pada hari Minggu ', '2021-11-10 13:52:23', '2021-11-10 13:52:23'),
(17, 'Lia', '1993-02-17', 'Kamu lahir pada hari Kamis ', '2021-11-11 00:31:58', '2021-11-11 00:31:58'),
(18, 'Isti Nur Khomalia', '2021-11-08', 'Kamu lahir pada hari Kamis ', '2021-11-13 23:45:28', '2021-11-13 23:45:28'),
(19, 'Toni Ja', '2021-11-03', 'Kamu lahir pada hari Minggu ', '2021-11-24 02:54:58', '2021-11-24 02:54:58');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitungbb`
--

CREATE TABLE `hitungbb` (
  `hitungbb_id` int(11) NOT NULL,
  `berat_badan` int(11) NOT NULL,
  `tinggi_badan` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `create_ad` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hitungbb`
--

INSERT INTO `hitungbb` (`hitungbb_id`, `berat_badan`, `tinggi_badan`, `keterangan`, `updated_at`, `create_ad`) VALUES
(1, 74, 167, 'BERAT BADAN ANDA TIDAK IDEAL !!!', '2021-10-23 22:43:13', '2021-10-23 22:43:13'),
(4, 74, 167, 'BERAT BADAN ANDA TIDAK IDEAL, KAMU GEMMBROOOTT HAHA', '2021-10-23 23:42:57', '2021-10-23 23:42:57'),
(5, 80, 156, 'BERAT BADAN ANDA TIDAK IDEAL, KAMU GEMMBROOOTT HAHA', '2021-10-23 23:44:41', '2021-10-23 23:44:41'),
(6, 64, 167, 'BERAT BADAN ANDA TIDAK IDEAL, KAMU GEMMBROOOTT HAHA', '2021-10-23 23:45:07', '2021-10-23 23:45:07'),
(7, 60, 167, 'BERAT BEDAN ANDA IDELA, OOYEEEACCH', '2021-10-23 23:45:17', '2021-10-23 23:45:17'),
(8, 55, 167, 'BERAT BADAN ANDA TIDAK IDEAL, KAMU GEMMBROOOTT HAHA', '2021-10-24 01:35:46', '2021-10-24 01:35:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitungkeliling`
--

CREATE TABLE `hitungkeliling` (
  `hitungkeliling_id` int(11) UNSIGNED NOT NULL,
  `luas` int(11) NOT NULL,
  `keliling` int(11) NOT NULL,
  `panjang` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitungluas`
--

CREATE TABLE `hitungluas` (
  `hitungluas_id` int(11) UNSIGNED NOT NULL,
  `luas` int(11) NOT NULL,
  `keliling` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hitungparkir`
--

CREATE TABLE `hitungparkir` (
  `hitungparkir_id` int(11) NOT NULL,
  `jenis_kendaraan` varchar(255) NOT NULL,
  `biaya_perjam` varchar(255) NOT NULL,
  `anda_parkir` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `hitungparkir`
--

INSERT INTO `hitungparkir` (`hitungparkir_id`, `jenis_kendaraan`, `biaya_perjam`, `anda_parkir`, `keterangan`, `created_at`, `updated_at`) VALUES
(4, 'MOTOR', 'Rp. 5000', '5 Hari', 'Terima kasih sayang sudah parkir dengan tertib', '2021-10-28 23:52:42', '2021-10-28 23:52:42'),
(5, 'SEPEDA', 'Rp. 2000', '10 Hari', 'Terima kasih sayang sudah parkir dengan tertib', '2021-10-28 23:53:08', '2021-10-28 23:53:08'),
(6, 'MOBIL', 'Rp. 10.000', '3 Hari', 'Terima kasih sayang sudah parkir dengan tertib', '2021-10-28 23:53:15', '2021-10-28 23:53:15'),
(7, 'BIS', 'Rp. 15.000', '4 Hari', 'Terima kasih sayang sudah parkir dengan tertib', '2021-10-28 23:53:21', '2021-10-28 23:53:21'),
(8, 'TRUK', 'Rp. 20.000', '7 Hari', 'Terima kasih sayang sudah parkir dengan tertib', '2021-10-28 23:53:28', '2021-10-28 23:53:28'),
(9, 'BIS', 'Rp. 15.000', '4 Hari', 'Terima kasih sayang sudah parkir dengan tertib', '2021-11-03 01:36:33', '2021-11-03 01:36:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jamkerja`
--

CREATE TABLE `jamkerja` (
  `jamkerja_id` int(11) NOT NULL,
  `jumlah_jam_kerja` int(11) NOT NULL,
  `gaji_anda` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jamkerja`
--

INSERT INTO `jamkerja` (`jamkerja_id`, `jumlah_jam_kerja`, `gaji_anda`, `created_at`, `updated_at`) VALUES
(1, 10, 1000000, '2021-10-23 16:17:14', '2021-10-23 16:17:14'),
(4, 12, 0, '2021-10-23 22:26:24', '2021-10-23 22:26:24'),
(5, 14, 0, '2021-10-23 22:26:31', '2021-10-23 22:26:31'),
(6, 12, 0, '2021-10-23 22:34:20', '2021-10-23 22:34:20'),
(7, 14, 0, '2021-11-02 01:54:00', '2021-11-02 01:54:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jamswitch`
--

CREATE TABLE `jamswitch` (
  `jamswitch_id` int(11) NOT NULL,
  `jumlah_jam_kerja` int(11) NOT NULL,
  `jam_lembur_anda` int(11) NOT NULL,
  `gol_kerja` varchar(255) NOT NULL,
  `gaji_anda` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jamswitch`
--

INSERT INTO `jamswitch` (`jamswitch_id`, `jumlah_jam_kerja`, `jam_lembur_anda`, `gol_kerja`, `gaji_anda`, `created_at`, `updated_at`) VALUES
(1, 48, 999952, 'A ', 3000, '2021-11-08 08:25:50', '2021-11-08 08:25:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jarakwaktu`
--

CREATE TABLE `jarakwaktu` (
  `jarakwaktu_id` int(11) NOT NULL,
  `kecepatan` int(11) NOT NULL,
  `jarak` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jarakwaktu`
--

INSERT INTO `jarakwaktu` (`jarakwaktu_id`, `kecepatan`, `jarak`, `waktu`, `created_at`, `updated_at`) VALUES
(1, 90, 500, 0, '2021-09-08 04:09:14', '0000-00-00 00:00:00'),
(4, 100, 500, 5, '2021-09-08 04:14:10', '0000-00-00 00:00:00'),
(6, 90, 1200, 13, '2021-09-08 08:37:13', '0000-00-00 00:00:00'),
(7, 130, 10000, 77, '2021-09-08 08:37:28', '0000-00-00 00:00:00'),
(8, 80, 240, 3, '2021-09-08 08:37:47', '0000-00-00 00:00:00'),
(9, 200, 1000, 5, '2021-09-08 08:38:02', '0000-00-00 00:00:00'),
(15, 100, 500, 5, '2021-09-08 08:40:16', '0000-00-00 00:00:00'),
(19, 100, 500, 5, '2021-10-23 01:31:17', '0000-00-00 00:00:00'),
(22, 100, 500, 5, '2021-10-23 09:17:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenispasien`
--

CREATE TABLE `jenispasien` (
  `jenispasien_id` int(11) UNSIGNED NOT NULL,
  `nama_jenis_pasien` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jenispasien`
--

INSERT INTO `jenispasien` (`jenispasien_id`, `nama_jenis_pasien`, `created_at`, `updated_at`) VALUES
(3, 'POLRI', '2021-09-22 02:40:24', '0000-00-00 00:00:00'),
(4, 'TNI', '2021-09-22 02:51:13', '0000-00-00 00:00:00'),
(5, 'NON TNI', '2021-09-22 02:51:24', '0000-00-00 00:00:00'),
(6, 'KELUARGA TNI', '2021-09-22 02:53:07', '0000-00-00 00:00:00'),
(7, 'PNS TNI', '2021-09-22 02:53:21', '0000-00-00 00:00:00'),
(8, 'KELUARGA PNS TNI', '2021-09-22 02:53:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jqform`
--

CREATE TABLE `jqform` (
  `jqform_id` int(11) NOT NULL,
  `no_rm` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `Jenis_kelamin` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `agama` varchar(255) NOT NULL,
  `gol_darah` varchar(255) NOT NULL,
  `pendiddikan` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE `kabupaten` (
  `kabupaten_id` bigint(20) UNSIGNED NOT NULL,
  `provinsi_id` bigint(20) UNSIGNED NOT NULL,
  `kota_id` bigint(20) UNSIGNED NOT NULL,
  `nama_kabupaten` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` int(9) NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kabupaten`
--

INSERT INTO `kabupaten` (`kabupaten_id`, `provinsi_id`, `kota_id`, `nama_kabupaten`, `image`, `email`, `kode_pos`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 82, 3, 'PATI', NULL, 'admin@gmail.com', 5, 'sego gandul', '2022-01-13 01:40:30', '2022-01-13 01:40:30'),
(2, 82, 5, 'GROBOGAN', NULL, 'dadi@gmail.com', 6, 'suwekey ', '2022-01-13 01:40:30', '2022-01-13 01:40:30'),
(3, 5, 4, 'SLEMAN', NULL, 'jogja@gmail.com', 0, 'nasi gudeg', '2022-01-13 01:40:30', '2022-01-13 01:40:30'),
(4, 83, 9, 'LAMONGAN', NULL, 'lam@gmail.com', 1, 'sambel pyet lamongan ', '2022-01-13 01:40:30', '2022-01-13 01:40:30'),
(5, 82, 10, 'KLATEN', NULL, 'klaten@gmail.com', 5, 'Eos amet quasi sunt occaecati hic est quidem qui maxime recusandae quis cupiditate sit ut earum facilis nam non voluptatem ut maxime occaecati voluptatum libero laborum error totam delectus quo modi vel velit adipisci consequatur.', '2022-01-13 01:40:30', '2022-01-13 01:40:30'),
(6, 82, 6, 'SURAKARTA', NULL, 'su@gmail.com', 8, 'Mollitia distinctio mollitia aliquam rerum labore nam voluptas vitae molestias nihil temporibus consequatur molestias sapiente numquam nihil rem ipsam asperiores odit aut aut ut vitae aut natus suscipit consequatur natus sequi modi at id minima expedita veniam dolores fugit in natus cumque tempore dolor cum magni harum aspernatur odit architecto rem iure facilis sit tempora voluptatem enim iusto porro non cumque harum officiis velit eum ipsum ducimus.', '2022-01-13 01:40:30', '2022-01-13 01:40:30'),
(7, 12, 2, 'CIREBON', NULL, 'ci@gmail.com', 0, 'Fugiat magnam at sint et nobis sed voluptatum voluptates est molestias numquam voluptatem quia tenetur fugiat vero porro vitae assumenda laboriosam recusandae quasi ea quia corporis tempora dolor omnis nulla voluptas harum provident ut repellendus autem dolor laudantium distinctio voluptatum quae ea ipsa pariatur quo iusto eligendi et esse.', '2022-01-13 01:40:30', '2022-01-13 01:40:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `karyawan_id` bigint(20) UNSIGNED NOT NULL,
  `cuti_id` int(11) NOT NULL,
  `no_induk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `tanggal_bergabung` date NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`karyawan_id`, `cuti_id`, `no_induk`, `nama`, `email`, `jenis_kelamin`, `image`, `alamat`, `tanggal_lahir`, `tanggal_bergabung`, `created_at`, `updated_at`) VALUES
(93, 52, '111212', 'Rahmat tengil', 'admin@perpus.test', 'Perempuan', '1650471065.jpg', 'Kalimantan', '1996-02-06', '2022-02-01', '2022-04-20 16:23:44', '2022-04-20 16:23:44'),
(107, 44, 'AB 10002 OP', 'Jefri', 'jefri@gmail.com', 'Perempuan', '1650471065.jpg', 'Sleman jakal km 7', '2020-02-07', '2022-03-06', '2022-04-20 16:23:48', '2022-04-20 16:23:48'),
(108, 45, 'B 0987 UT', 'ami', 'ami@gmail.com', 'Laki-laki', '1650471065.jpg', 'bvhdf vh', '2022-03-03', '2022-03-08', '2022-04-20 16:23:51', '2022-04-20 16:23:51'),
(109, 46, 'bg7868', 'maaa', 'admin@mail.com', 'Laki-laki', '1650471065.jpg', 'g ncv xcb ggfb 567567', '2022-03-01', '2022-03-03', '2022-04-20 16:23:54', '2022-04-20 16:23:54'),
(113, 53, 'ABC 123', 'Pratama Update', 'pratama@gmail.com', 'Perempuan', '1650505272.png', 'UII Jogja', '2022-04-06', '2022-04-19', '2022-04-21 01:41:12', '2022-04-21 01:41:12'),
(114, 54, 'we33333', 'NURASYID. TN', 'admin@mail.com', 'Laki-laki', '1650495291.png', 'gbgbg', '2022-04-08', '2022-04-08', '2022-04-20 22:54:51', '2022-04-20 22:54:51'),
(115, 58, '12345678', 'An Nisa Az Zahra', 'zahra@gmail.com', 'Perempuan', '1650682194.jpg', 'Annisa Zahra', '2022-04-14', '2022-04-23', '2022-04-23 02:49:54', '2022-04-23 02:49:54'),
(117, 50, '1234', 'defrii ji', 'amirinsyaifudin6@gmail.com', 'Laki-laki', '1650507324.jpeg', 'defri', '2022-04-20', '2022-04-13', '2022-04-21 02:15:24', '2022-04-21 02:15:24'),
(118, 50, '00000988', 'nanana', 'amirinsyaifudin6@gmail.com', 'Laki-laki', '1650507300.png', 'nanananna', '2022-04-20', '2022-04-13', '2022-04-21 02:15:00', '2022-04-21 02:15:00'),
(121, 55, 'KM 234', 'Aryo', 'aryo@gmail.com', 'Laki-laki', '1650506712.png', 'aryo', '2022-04-11', '2022-04-10', '2022-04-21 02:05:12', '2022-04-21 02:05:12'),
(124, 51, '977bhb', 'Dismas', 'dismas@gmail.com', 'Perempuan', '1650505767.jpg', 'kayen', '2022-04-04', '0000-00-00', '2022-04-21 01:49:27', '2022-04-21 01:49:27'),
(125, 50, 'QWE12389', 'Silva', 'silva@gmail.com', 'Perempuan', '1650507427.jpg', 'Jogja 324356', '2022-04-07', '0000-00-00', '2022-04-21 02:17:07', '2022-04-21 02:17:07'),
(127, 59, 'AW2556', 'Muhammad Salman', 'salman@gmail.com', 'Laki-laki', '1650682268.jpg', 'Salman King', '2022-04-22', '0000-00-00', '2022-04-23 02:51:08', '2022-04-23 02:51:08'),
(128, 51, 'ANS 1245', 'Anis Lail', 'laili@gmail.com', 'Perempuan', '1652678734.jpeg', 'Purwodadi 12345', '2022-05-11', '0000-00-00', '2022-05-16 05:25:34', '2022-05-16 05:25:34'),
(131, 51, '125610132', 'Amirin Syaifudin', 'amirinsyaifudin6@gmail.com', 'Laki-laki', '1658532938.png', 'Ds. Sukolilo \r\nRT/RW = 04/08\r\nKec. Sukolilo \r\nKab. PATI \r\nJAWA TENGAH', '2022-07-29', '0000-00-00', '2022-07-22 23:35:38', '2022-07-22 23:35:38'),
(132, 51, '125610123', 'An Nisa Walidatus Sholihah', 'annisawalidatus@gmail.com', 'Perempuan', '1658533021.png', 'JunRejo. Kota Batu, Malang, JAWA TIMUR', '2022-07-29', '0000-00-00', '2022-07-22 23:37:01', '2022-07-22 23:37:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_cuti`
--

CREATE TABLE `kategori_cuti` (
  `kategori_cuti_id` int(11) NOT NULL,
  `nama_cuti` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kota`
--

CREATE TABLE `kota` (
  `kota_id` bigint(20) UNSIGNED NOT NULL,
  `provinsi_id` bigint(20) UNSIGNED NOT NULL,
  `nama_kota` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kota`
--

INSERT INTO `kota` (`kota_id`, `provinsi_id`, `nama_kota`, `kode_pos`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 82, 'PATI', '2', 'Sit ut laudantium et doloribus. Sint impedit qui voluptatum nihil placeat. Ut voluptatem voluptatem est ut. Necessitatibus labore dolores ad est facere vero.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(2, 82, 'KUDUS', '7', 'Temporibus repellat repellendus rerum non sed. Rerum inventore dolor quis aut laborum. Quae adipisci qui architecto nemo nihil. Repellendus enim voluptas sunt repellendus quod doloribus quia.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(3, 12, 'CIREBON', '2', 'Sunt dolor aliquam quo ducimus vitae dolores ab. Sunt nostrum atque quod non esse tempora. Animi voluptatem non dolorum assumenda architecto.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(4, 83, 'BEKASI', '9', 'Numquam accusamus vero dolor. Voluptates quibusdam voluptatibus vitae at quaerat qui. Qui adipisci ullam neque reiciendis expedita.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(5, 82, 'SURAKARTA', '9', 'Deleniti sit necessitatibus eum reiciendis est est. Dolor corrupti praesentium veritatis similique. Harum labore recusandae cupiditate modi.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(6, 5, 'GUNUNGKIDUL', '9', 'Qui recusandae labore quibusdam quaerat in. Mollitia doloremque doloribus quia ad. Tenetur voluptatem earum asperiores quidem magni ex.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(7, 82, 'KENDAL ', '5', 'Autem recusandae sed placeat nam. Nostrum rem cupiditate qui ipsum. Eaque dolore et quam quas exercitationem et labore aperiam. Consequatur enim tempore ducimus eum itaque.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(9, 82, 'MAGELANG', '8', 'Dicta non labore eos. Sed nobis magni sapiente debitis voluptatem ut porro. Aut atque atque quia laborum in. Id nesciunt ipsa autem eos.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(10, 82, 'KLATEN', '0', 'Odit nostrum sit voluptate distinctio cumque. Distinctio dolorem quidem totam tempora dolores. Aut quia quod est aut numquam.', '2022-01-13 01:40:14', '2022-01-13 01:40:14'),
(26, 5, 'KULON PROGO', '1', 'Eos mollitia corrupti nulla dolorem repellat saepe. Quam perspiciatis sunt et id. Repellendus non consequatur sed suscipit. Pariatur enim enim animi aliquid quis consequatur.', '2022-01-13 09:33:07', '2022-01-13 09:33:07'),
(29, 5, 'SLEMAN ', '1', 'Ut at quia quisquam itaque officiis ut architecto. Pariatur ipsum blanditiis libero saepe. Natus ad error illum magnam assumenda et dignissimos perspiciatis. Illo omnis ipsum maiores.', '2022-01-13 09:33:07', '2022-01-13 09:33:07'),
(37, 82, 'gtgtgtgtgttgtg', '6547647', 'hnhnhnh', '2022-04-20 23:59:45', '2022-04-20 23:59:45'),
(38, 83, 'Nyawi', '098778', 'Nyawi Jatim', '2022-04-21 02:02:23', '2022-04-21 02:02:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `listkerjaan`
--

CREATE TABLE `listkerjaan` (
  `listkerjaan_id` int(20) NOT NULL,
  `nama_kerjaan` text NOT NULL,
  `issue` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `teknologi` varchar(255) NOT NULL,
  `ada_git` varchar(255) NOT NULL,
  `ada_github` varchar(255) NOT NULL,
  `tanggal_issue` date NOT NULL,
  `nama_mentor` varchar(255) NOT NULL,
  `referensi` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2019_08_19_000000_create_failed_jobs_table', 1),
(16, '2020_05_12_061444_create_permission_tables', 1),
(17, '2020_11_07_221923_create_kategori_table', 1),
(18, '2020_11_07_222114_create_produk_table', 1),
(19, '2020_11_07_222158_create_pages_table', 1),
(20, '2020_11_07_222242_create_member_table', 1),
(21, '2020_11_07_222330_create_menu_table', 1),
(22, '2020_11_07_222415_create_menu_admin_table', 1),
(23, '2020_11_07_222451_create_transaksi_table', 1),
(24, '2020_11_07_222518_create_transaksi_detail_table', 1),
(25, '2020_11_11_125730_create_katagori_table', 2),
(26, '2020_11_11_130226_create_produk_table', 2),
(27, '2020_11_11_131308_create_member_table', 2),
(28, '2020_11_11_132141_create_saran_table', 2),
(29, '2020_11_12_011816_create_komentaranda_table', 2),
(30, '2020_11_12_020903_create_transaksi_table', 3),
(31, '2020_11_12_065816_create_customer_table', 4),
(32, '2020_11_12_071328_create_customer_table', 5),
(33, '2020_11_17_023824_create_provinsi_table', 6),
(34, '2020_11_17_023837_create_kota_table', 6),
(35, '2020_11_17_034307_create_kabupaten_table', 7),
(36, '2020_11_17_034633_create_order_table', 8),
(37, '2020_11_17_035457_create_orderdetail_table', 9),
(38, '2020_11_17_060614_create_transaksi_table', 10),
(39, '2020_11_17_060731_create_transaksidetail_table', 10),
(40, '2020_11_19_012059_create_transaksidetail_table', 11),
(41, '2021_03_19_063024_create_cuti_table', 12),
(42, '2021_03_20_055854_create_tablekaryawan_table', 13),
(43, '2021_08_10_054512_create_fungsi_terbilang_table', 14),
(44, '2022_01_12_170604_create_provinsi_table', 14),
(45, '2022_01_12_170841_create_kota_table', 14),
(46, '2022_01_12_170901_create_kabupaten_table', 14),
(47, '2022_01_31_180138_create_employees_table', 15);

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 2),
(1, 'App\\User', 3),
(1, 'App\\User', 4),
(1, 'App\\User', 6),
(2, 'App\\User', 5),
(2, 'App\\User', 7),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 11),
(2, 'App\\User', 12),
(2, 'App\\User', 13);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilailulus`
--

CREATE TABLE `nilailulus` (
  `nilailulus_id` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `index_prestasi` varchar(255) NOT NULL,
  `lulus_tahun` varchar(255) NOT NULL,
  `predikat` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `nilailulus`
--

INSERT INTO `nilailulus` (`nilailulus_id`, `nilai`, `keterangan`, `index_prestasi`, `lulus_tahun`, `predikat`, `created_at`, `updated_at`) VALUES
(1, 90, 'lulus', 'A', '2016', 'lululusan terbaik ', '2021-10-28 23:32:42', '2021-10-28 23:32:42'),
(4, 100, 'LULUS tepat waktu', 'IPK 3.8', '3.5 Tahun', 'Clum load', '2021-10-29 00:13:17', '2021-10-29 00:13:17'),
(5, 40, 'LULUS tepat ', 'IPK 2.5', '5 Tahun', 'Tidak Clum load', '2021-10-29 00:13:29', '2021-10-29 00:13:29'),
(6, 72, 'LULUS tepat waktu', 'IPK 3.6', '5 Tahun', 'Hampir Clum load', '2021-10-29 00:13:44', '2021-10-29 00:13:44'),
(7, 70, 'LULUS tepat waktu', 'IPK 3.6', '5 Tahun', 'Hampir Clum load', '2021-11-03 01:29:26', '2021-11-03 01:29:26'),
(8, 100, 'LULUS tepat waktu', 'IPK 3.8', '3.5 Tahun', 'Clum load', '2021-11-03 01:29:32', '2021-11-03 01:29:32'),
(9, 90, 'LULUS tepat waktu', 'IPK 3.8', '3.5 Tahun', 'Clum load', '2021-11-03 01:29:49', '2021-11-03 01:29:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilaimhs`
--

CREATE TABLE `nilaimhs` (
  `nilaimhs_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `mata_kuliah` varchar(255) NOT NULL,
  `nilai_absen` int(11) NOT NULL,
  `nilai_tugas` int(11) NOT NULL,
  `nilai_uts` int(11) NOT NULL,
  `nilai_uas` int(11) NOT NULL,
  `nilai_akhir` int(11) NOT NULL,
  `grade` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `nilaimhs`
--

INSERT INTO `nilaimhs` (`nilaimhs_id`, `nama`, `mata_kuliah`, `nilai_absen`, `nilai_tugas`, `nilai_uts`, `nilai_uas`, `nilai_akhir`, `grade`, `created_at`, `updated_at`) VALUES
(10, 'Robby Pratama', 'Pyton', 90, 80, 90, 95, 27, 'E', '2021-10-27 14:18:45', '2021-10-27 14:18:45'),
(11, 'NURASYID. TN', 'gyuguy', 90, 80, 90, 95, 81, 'A', '2021-10-27 14:21:12', '2021-10-27 14:21:12'),
(12, 'Yusuf Ma', 'Pemrogramman PHP', 100, 100, 100, 100, 90, 'A', '2021-10-28 03:25:21', '2021-10-28 03:25:21'),
(14, 'Sulaiman Saladin Uno', 'Sastra Prancis', 85, 85, 85, 85, 77, 'B', '2021-10-28 03:27:25', '2021-10-28 03:27:25'),
(15, 'Amir Malik', 'Kubernetas', 95, 95, 95, 95, 86, 'A', '2021-10-28 03:39:28', '2021-10-28 03:39:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `parkir`
--

CREATE TABLE `parkir` (
  `parkir_id` int(11) NOT NULL,
  `no_parkir` varchar(255) NOT NULL,
  `jenis_kendaraan` varchar(255) NOT NULL,
  `napol` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tarif_perjam` int(11) NOT NULL,
  `total_tahun` int(11) NOT NULL,
  `total_bulan` int(11) NOT NULL,
  `total_hari` int(11) NOT NULL,
  `total_jam` int(11) NOT NULL,
  `total_menit` int(11) NOT NULL,
  `total_detik` int(11) NOT NULL,
  `total_bayar` int(11) NOT NULL,
  `anda_parkir` varchar(255) NOT NULL,
  `tanggal_jam_masuk` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `parkir`
--

INSERT INTO `parkir` (`parkir_id`, `no_parkir`, `jenis_kendaraan`, `napol`, `status`, `tarif_perjam`, `total_tahun`, `total_bulan`, `total_hari`, `total_jam`, `total_menit`, `total_detik`, `total_bayar`, `anda_parkir`, `tanggal_jam_masuk`, `created_at`, `updated_at`) VALUES
(24, 'MTR-202110202213', 'SEPEDA', 'AB 8980 KY', 'AKTIF', 2000, 0, 0, 0, 0, 0, 0, 0, '', '2021-04-01 09:11:02', '2021-11-24 14:17:27', '2021-11-24 14:17:27'),
(25, 'MTR-202110202214', 'MOTOR', 'AB Selman 123', 'AKTIF', 5000, 0, 0, 0, 0, 0, 0, 0, '', '2021-11-23 09:11:02', '2021-10-29 03:50:18', '0000-00-00 00:00:00'),
(26, 'MTR-202110202215', 'BIS', 'B 345 Sleman', 'AKTIF', 15000, 0, 0, 0, 0, 0, 0, 0, '', '2021-11-23 09:11:02', '2021-10-29 03:50:40', '0000-00-00 00:00:00'),
(27, 'MTR-202110202216', 'TRUK', 'AA 234 SK', 'AKTIF', 20000, 0, 0, 0, 0, 0, 0, 0, '', '2021-11-23 09:11:02', '2021-10-29 03:51:09', '0000-00-00 00:00:00'),
(28, 'MTR-202110202217', 'MOTOR', 'T 09890 NO', 'AKTIF', 5000, 0, 0, 0, 0, 0, 0, 0, '', '2021-11-23 09:11:02', '2021-10-29 03:53:36', '0000-00-00 00:00:00'),
(30, 'MTR-202110202218', 'SEPEDA', 'AB Jogja 123', 'AKTIF', 2000, 0, 0, 0, 0, 0, 0, 0, '', '2021-11-23 09:11:02', '2021-10-29 08:37:15', '0000-00-00 00:00:00'),
(37, 'MTR-202111202221', 'TRUK', 'AB098Yt', 'AKTIF', 20000, 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00 00:00:00', '2021-11-24 16:36:59', '0000-00-00 00:00:00'),
(38, 'MTR-202111202222', 'BIS', 'K 0987 YT', 'AKTIF', 15000, 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00 00:00:00', '2021-11-24 16:37:23', '0000-00-00 00:00:00'),
(39, 'MTR-202111202223', 'MOBIL', 'P 9876 IP', 'AKTIF', 10000, 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00 00:00:00', '2021-11-24 16:37:35', '0000-00-00 00:00:00'),
(40, 'MTR-202111202224', 'MOTOR', 'S 0987 YT', 'AKTIF', 5000, 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00 00:00:00', '2021-11-24 16:37:52', '0000-00-00 00:00:00'),
(41, 'MTR-202111202225', 'SEPEDA', 'O 9870 UY', 'AKTIF', 2000, 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00 00:00:00', '2021-11-24 16:38:04', '0000-00-00 00:00:00'),
(42, 'MTR-202111202226', 'SEPEDA', 'B 9874 WO', 'AKTIF', 500, 0, 0, 0, 0, 0, 0, 0, '', '0000-00-00 00:00:00', '2021-11-25 05:23:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `pasien_id` int(11) UNSIGNED NOT NULL,
  `jenispasien_id` int(10) UNSIGNED NOT NULL,
  `poliklinik_id` int(11) UNSIGNED NOT NULL,
  `agama_id` int(11) UNSIGNED NOT NULL,
  `golongan_darah_id` int(11) UNSIGNED NOT NULL,
  `pekerjaan_id` int(11) UNSIGNED NOT NULL,
  `pendidikan_id` int(11) UNSIGNED NOT NULL,
  `status_perkawinan_id` int(11) UNSIGNED NOT NULL,
  `suku_id` int(11) UNSIGNED NOT NULL,
  `cover` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rekam_medik` int(11) NOT NULL,
  `nama_pasien` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`pasien_id`, `jenispasien_id`, `poliklinik_id`, `agama_id`, `golongan_darah_id`, `pekerjaan_id`, `pendidikan_id`, `status_perkawinan_id`, `suku_id`, `cover`, `no_rekam_medik`, `nama_pasien`, `tanggal_lahir`, `created_at`, `updated_at`) VALUES
(8, 6, 2, 3, 1, 9, 13, 6, 1, NULL, 1112187, 'Roman Advin', '1995-02-14', '2021-11-24 02:50:45', '2021-11-24 02:50:45'),
(9, 5, 1, 1, 1, 1, 1, 1, 1, NULL, 668567, 'Vita', '1994-06-07', '2021-11-26 03:01:35', '2021-11-26 03:01:35'),
(11, 7, 8, 1, 6, 16, 6, 1, 1, NULL, 675468, 'Yusuf Alafasy', '1993-05-02', '2021-11-26 03:14:33', '2021-11-26 03:14:33'),
(13, 3, 1, 1, 1, 1, 1, 1, 1, 'assets/covers/e9q4VJweaTqmGpdGws8KsxBIBR0TTDazv6zhATUB.jpg', 13334545, 'aaa coba', '2022-02-01', '2022-02-02 22:04:39', '2022-02-02 22:04:39'),
(14, 3, 1, 1, 1, 1, 1, 1, 1, 'assets/covers/gnWP7jJUgw3sSC7sF2eTEw2ThANsMG76C5MJ6VCg.jpg', 999888, 'kaji edan', '2022-01-30', '2022-02-02 22:16:28', '2022-02-02 22:16:28'),
(15, 3, 1, 1, 1, 1, 1, 1, 1, 'assets/covers/dTRrOuUNElLjlPJqrYEaV5QtJs3us52UJACMqp8b.png', 2147483647, 'fefefef', '2022-01-31', '2022-02-02 22:19:48', '2022-02-02 22:19:48'),
(16, 3, 1, 1, 1, 1, 1, 1, 1, 'assets/covers/SLp4JJ5LX2HAzjMvzVKEZdt9cfkrG5zIFA5eRtno.jpg', 845768, 'Nuur', '2022-02-01', '2022-02-03 08:45:42', '2022-02-03 08:45:42'),
(17, 3, 1, 1, 1, 1, 1, 1, 1, 'assets/covers/JJkmO9P3cqOFROfvHnL2FRAMvnBAE4XNkeOdO22I.jpg', 845768, 'Ahmad', '2009-06-16', '2022-02-03 08:47:04', '2022-02-03 08:47:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `pekerjaan_id` int(11) NOT NULL,
  `nama_pekerjaan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pekerjaan`
--

INSERT INTO `pekerjaan` (`pekerjaan_id`, `nama_pekerjaan`, `created_at`, `updated_at`) VALUES
(1, 'AGEN', '2021-09-22 04:30:58', '0000-00-00 00:00:00'),
(2, 'ADMINISTRASI', '2021-09-22 04:30:46', '0000-00-00 00:00:00'),
(3, 'ADVOKAT', '2021-09-22 04:30:53', '0000-00-00 00:00:00'),
(5, 'AKUTANSI', '2021-09-22 04:31:25', '0000-00-00 00:00:00'),
(6, 'APOTEKER', '2021-09-22 04:31:32', '0000-00-00 00:00:00'),
(7, 'SWASTA', '2021-09-22 04:31:49', '0000-00-00 00:00:00'),
(8, 'POLISI', '2021-09-22 04:31:56', '0000-00-00 00:00:00'),
(9, 'TNI', '2021-09-22 04:32:03', '0000-00-00 00:00:00'),
(10, 'PRESIDEN', '2021-09-22 04:32:10', '0000-00-00 00:00:00'),
(11, 'WAKIL PRESIDEN', '2021-09-22 04:32:20', '0000-00-00 00:00:00'),
(12, 'GUBERNUR', '2021-09-22 04:32:34', '0000-00-00 00:00:00'),
(13, 'WAKIL GUBERNUR', '2021-09-22 04:32:44', '0000-00-00 00:00:00'),
(14, 'WALIKOTA', '2021-09-22 04:32:50', '0000-00-00 00:00:00'),
(15, 'WAKIL WALIKOTA', '2021-09-22 04:33:00', '0000-00-00 00:00:00'),
(16, 'BUPATI', '2021-09-22 04:33:09', '0000-00-00 00:00:00'),
(17, 'WAKIL BUPATI', '2021-09-22 04:33:17', '0000-00-00 00:00:00'),
(18, 'GURU HONORER', '2021-09-22 04:33:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembagian`
--

CREATE TABLE `pembagian` (
  `pembagian_id` int(11) NOT NULL,
  `bil_satu` int(11) NOT NULL,
  `bil_dua` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `fungsi_terbilang` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pembagian`
--

INSERT INTO `pembagian` (`pembagian_id`, `bil_satu`, `bil_dua`, `jumlah`, `fungsi_terbilang`, `created_at`, `updated_at`) VALUES
(2, 100, 2, 50, '', NULL, NULL),
(3, 10, 5, 2, '', NULL, NULL),
(4, 100, 8, 13, '', NULL, NULL),
(5, 7, 3, 2, '', NULL, NULL),
(6, 10, 3, 3, '', NULL, NULL),
(7, 10, 3, 3, '', NULL, NULL),
(8, 1000, 3, 333, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembagian_js`
--

CREATE TABLE `pembagian_js` (
  `pembagianjs_id` int(11) NOT NULL,
  `bil_satu` varchar(255) NOT NULL,
  `bil_dua` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `fungsi_terbilang` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendidikan`
--

CREATE TABLE `pendidikan` (
  `pendidikan_id` int(11) NOT NULL,
  `nama_pendidikan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pendidikan`
--

INSERT INTO `pendidikan` (`pendidikan_id`, `nama_pendidikan`, `created_at`, `updated_at`) VALUES
(1, 'S1', '2021-09-22 04:16:51', '0000-00-00 00:00:00'),
(2, 'SMP', '2021-09-22 04:16:40', '0000-00-00 00:00:00'),
(3, 'SMA', '2021-09-22 04:16:46', '0000-00-00 00:00:00'),
(5, 'S2', '2021-09-22 04:16:56', '0000-00-00 00:00:00'),
(6, 'S3', '2021-09-22 04:17:01', '0000-00-00 00:00:00'),
(7, 'TIDAK SEKOLAH', '2021-09-22 04:17:11', '0000-00-00 00:00:00'),
(8, 'AKABRI', '2021-09-22 04:17:41', '0000-00-00 00:00:00'),
(9, 'AKPER', '2021-09-22 04:17:47', '0000-00-00 00:00:00'),
(10, 'AKADEMI', '2021-09-22 04:18:02', '0000-00-00 00:00:00'),
(11, 'D1', '2021-09-22 04:18:18', '0000-00-00 00:00:00'),
(12, 'D2', '2021-09-22 04:18:35', '0000-00-00 00:00:00'),
(13, 'D3', '2021-09-22 04:18:40', '0000-00-00 00:00:00'),
(14, 'D4', '2021-09-22 04:18:46', '0000-00-00 00:00:00'),
(15, 'MA', '2021-09-22 04:19:12', '0000-00-00 00:00:00'),
(16, 'MI', '2021-09-22 04:19:18', '0000-00-00 00:00:00'),
(17, 'MTS', '2021-09-22 04:19:24', '0000-00-00 00:00:00'),
(18, 'PGSD', '2021-09-22 04:20:02', '0000-00-00 00:00:00'),
(19, 'PGTK', '2021-09-22 04:20:09', '0000-00-00 00:00:00'),
(20, 'NERS', '2021-09-22 04:20:26', '0000-00-00 00:00:00'),
(21, 'PGAN', '2021-09-22 04:20:33', '0000-00-00 00:00:00'),
(22, 'PT', '2021-09-22 04:20:50', '0000-00-00 00:00:00'),
(23, 'SARJANA', '2021-09-22 04:20:57', '0000-00-00 00:00:00'),
(24, 'TK', '2021-09-22 04:21:19', '0000-00-00 00:00:00'),
(25, 'SMEA', '2021-09-22 04:21:25', '0000-00-00 00:00:00'),
(26, 'SMK', '2021-09-22 04:21:30', '0000-00-00 00:00:00'),
(27, 'SD', '2021-09-22 04:23:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengurangan`
--

CREATE TABLE `pengurangan` (
  `pengurangan_id` int(11) NOT NULL,
  `bil_satu` int(11) NOT NULL,
  `bil_dua` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `fungsi_terbilang` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengurangan`
--

INSERT INTO `pengurangan` (`pengurangan_id`, `bil_satu`, `bil_dua`, `jumlah`, `fungsi_terbilang`, `created_at`, `updated_at`) VALUES
(3, 160, 100, 60, '', NULL, NULL),
(4, 100, 90, 10, '', NULL, NULL),
(5, 9, 8, 1, '', NULL, NULL),
(6, 8, 5, 3, '', NULL, NULL),
(7, 1000000000, 1000, 999999000, '', NULL, NULL),
(8, 100000000, 10000, 99990000, '', NULL, NULL),
(9, 100, 10, 90, '', NULL, NULL),
(10, 10000, 1500, 8500, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengurangan_js`
--

CREATE TABLE `pengurangan_js` (
  `penguranganjs_id` int(11) NOT NULL,
  `bil_satu` varchar(255) NOT NULL,
  `bil_dua` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `fungsi_terbilang` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjumlahan`
--

CREATE TABLE `penjumlahan` (
  `penjumlahan_id` int(11) UNSIGNED NOT NULL,
  `bil_satu` int(11) NOT NULL,
  `bil_dua` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `fungsi_terbilang` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penjumlahan`
--

INSERT INTO `penjumlahan` (`penjumlahan_id`, `bil_satu`, `bil_dua`, `jumlah`, `fungsi_terbilang`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 4, 'empat', NULL, NULL),
(2, 1, 1, 1, '', NULL, NULL),
(4, 1000, 1000, 2000, '', NULL, NULL),
(6, 10, 10, 20, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penjumlahan_js`
--

CREATE TABLE `penjumlahan_js` (
  `pejumlahanjs_id` int(11) NOT NULL,
  `bil_satu` varchar(255) NOT NULL,
  `bil_dua` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `fungsi_terbilang` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perkalian`
--

CREATE TABLE `perkalian` (
  `perkalian_id` int(11) NOT NULL,
  `bil_satu` int(11) NOT NULL,
  `bil_dua` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `fungsi_terbilang` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `perkalian`
--

INSERT INTO `perkalian` (`perkalian_id`, `bil_satu`, `bil_dua`, `jumlah`, `fungsi_terbilang`, `created_at`, `updated_at`) VALUES
(1, 8, 8, 64, '', NULL, NULL),
(2, 11, 11, 121, '', NULL, NULL),
(4, 100000, 100, 10000000, '', NULL, NULL),
(5, 100, 10, 1000, '', NULL, NULL),
(6, 9, 9, 81, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `perkalian_js`
--

CREATE TABLE `perkalian_js` (
  `perkalianjs_id` int(11) NOT NULL,
  `bil_satu` varchar(255) NOT NULL,
  `bil_dua` varchar(255) NOT NULL,
  `jumlah` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perusahaan`
--

CREATE TABLE `perusahaan` (
  `perusahaan_id` bigint(20) UNSIGNED NOT NULL,
  `karyawan_id` bigint(20) UNSIGNED NOT NULL,
  `cuti_id` bigint(20) UNSIGNED NOT NULL,
  `nama_company` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_gabung_karyawan` date NOT NULL,
  `lama_cuti_karyawan` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `poliklinik`
--

CREATE TABLE `poliklinik` (
  `poliklinik_id` int(11) NOT NULL,
  `nama_poliklinik` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `poliklinik`
--

INSERT INTO `poliklinik` (`poliklinik_id`, `nama_poliklinik`, `created_at`, `updated_at`) VALUES
(1, 'UGD', '2021-09-22 02:56:22', '0000-00-00 00:00:00'),
(2, 'LABORATORIUM', '2021-09-22 03:06:14', '0000-00-00 00:00:00'),
(3, 'RADIOLOGI', '2021-09-22 03:06:22', '0000-00-00 00:00:00'),
(5, 'POLI BEDAH', '2021-09-22 03:06:35', '0000-00-00 00:00:00'),
(6, 'POLI GIGI', '2021-09-22 03:06:42', '0000-00-00 00:00:00'),
(7, 'POLI MATA', '2021-09-22 03:06:50', '0000-00-00 00:00:00'),
(8, 'MCU', '2021-09-22 03:07:26', '0000-00-00 00:00:00'),
(9, 'POLIKLINIK BEDAH', '2021-09-22 03:07:35', '0000-00-00 00:00:00'),
(10, 'POLIKLINIK ANAK', '2021-09-22 03:07:45', '0000-00-00 00:00:00'),
(11, 'POLIKLINIK JIWA', '2021-09-22 03:08:13', '0000-00-00 00:00:00'),
(12, 'POLIKLINIK PENYAKIT DALAM', '2021-09-22 03:08:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_pribadi`
--

CREATE TABLE `profil_pribadi` (
  `profil_pribadi_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nip` varchar(255) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `tampat_tanggal_lahir` datetime NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `golongan_darah` varchar(255) NOT NULL,
  `alamat_ktp` varchar(255) NOT NULL,
  `alamat_domisili` varchar(255) NOT NULL,
  `status_pernikahan` varchar(255) NOT NULL,
  `provinsi_ktp` varchar(255) NOT NULL,
  `kabupaten_ktp` varchar(255) NOT NULL,
  `kecapatan_ktp` varchar(255) NOT NULL,
  `kelurahan_ktp` varchar(255) NOT NULL,
  `no_telpon` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `provinsi_id` bigint(20) UNSIGNED NOT NULL,
  `nama_provinsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_jadi_provinsi` date NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`provinsi_id`, `nama_provinsi`, `tanggal_jadi_provinsi`, `keterangan`, `created_at`, `updated_at`) VALUES
(5, 'DIY YOGYAKARTA', '1985-11-26', 'EDIT', '2022-01-13 01:38:48', '2022-01-13 01:38:48'),
(6, 'DKI JAKARTA', '1978-10-13', 'DKI', '2022-01-13 01:38:48', '2022-01-13 01:38:48'),
(12, 'JAWA BARAT', '1973-02-14', 'PART 1 fgb dd', NULL, NULL),
(82, 'JAWA TENGAH ', '1971-01-25', 'DKI', '2022-01-13 01:38:48', '2022-01-13 01:38:48'),
(83, 'JAWA TIMUR', '2020-04-01', 'Jatim', '2022-01-13 01:38:48', '2022-01-13 01:38:48'),
(84, 'BANTEN', '1993-07-15', 'Banten', '2022-01-13 01:38:48', '2022-01-13 01:38:48'),
(88, 'SUMATRA UTARA', '2022-01-02', 'SUMATRA UTARA OKE', '2022-01-31 02:52:21', '2022-01-31 02:52:21'),
(93, 'PAPUA TENGAH', '2022-03-07', 'PAPUA', '2022-03-08 00:07:12', '2022-03-08 00:07:12'),
(94, 'NTT', '2022-03-04', 'NUSA TENGGARA TIMUR', '2022-03-08 00:11:11', '2022-03-08 00:11:11'),
(95, 'NTB', '2022-03-01', 'NUSA TENGGARA BARAT', '2022-03-21 00:03:56', '2022-03-21 00:03:56'),
(96, 'SUMATRA SELATAN', '2022-03-25', 'SUMATRA', '2022-03-29 23:23:00', '2022-03-29 23:23:00'),
(99, 'Kebumen', '2022-05-06', 'fvffvfv', '2022-05-04 23:49:57', '2022-05-04 23:49:57'),
(100, 'MALUKU UTARA', '2022-05-01', 'MALUKU UTARA', '2022-05-22 03:59:34', '2022-05-22 03:59:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-11-08 07:03:29', '2020-11-08 07:03:29'),
(2, 'user', 'web', '2020-11-08 07:03:29', '2020-11-08 07:03:29'),
(3, 'member', 'web', '2020-11-08 07:03:29', '2020-11-08 07:03:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `status_perkawinan`
--

CREATE TABLE `status_perkawinan` (
  `status_perkawinan_id` int(11) NOT NULL,
  `nama_status_perkawinan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `status_perkawinan`
--

INSERT INTO `status_perkawinan` (`status_perkawinan_id`, `nama_status_perkawinan`, `created_at`, `updated_at`) VALUES
(1, 'LAJANG', '2021-09-22 04:04:34', '0000-00-00 00:00:00'),
(2, 'JANDA KEMBANG', '2021-09-22 04:04:15', '0000-00-00 00:00:00'),
(3, 'DUDA', '2021-09-22 04:04:26', '0000-00-00 00:00:00'),
(5, 'BELUM MENIKAH', '2021-09-22 04:04:58', '0000-00-00 00:00:00'),
(6, 'MENIKAH', '2021-09-22 04:07:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suku`
--

CREATE TABLE `suku` (
  `suku_id` int(11) NOT NULL,
  `nama_suku` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `suku`
--

INSERT INTO `suku` (`suku_id`, `nama_suku`, `created_at`, `updated_at`) VALUES
(1, 'JAWA TENGAH', '2021-09-22 05:09:30', '0000-00-00 00:00:00'),
(2, 'SUNDA', '2021-09-22 05:09:38', '0000-00-00 00:00:00'),
(3, 'BUGIS', '2021-09-22 05:09:46', '0000-00-00 00:00:00'),
(7, 'MELAYU', '2021-09-22 05:09:53', '0000-00-00 00:00:00'),
(8, 'DAYAK', '2021-09-22 05:09:59', '0000-00-00 00:00:00'),
(9, 'BATAK', '2021-09-22 05:10:05', '0000-00-00 00:00:00'),
(10, 'PAPUA', '2021-09-22 05:10:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahunkabisat`
--

CREATE TABLE `tahunkabisat` (
  `tahunkabisat_id` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tahunkabisat`
--

INSERT INTO `tahunkabisat` (`tahunkabisat_id`, `tahun`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1999, 'Tahun Kabisat ', '2021-10-23 14:20:21', '2021-10-23 14:20:21'),
(4, 1993, 'ADALAH BUKAN TAHUN KABISAT', '2021-10-23 15:56:11', '2021-10-23 15:56:11'),
(5, 1998, 'ADALAH BUKAN TAHUN KABISAT', '2021-10-23 15:56:45', '2021-10-23 15:56:45'),
(6, 2000, 'ADALAH TAHUN KABISAT', '2021-10-23 15:56:58', '2021-10-23 15:56:58'),
(7, 2001, 'ADALAH BUKAN TAHUN KABISAT', '2021-10-23 15:57:43', '2021-10-23 15:57:43'),
(8, 2008, 'ADALAH TAHUN KABISAT', '2021-10-23 15:58:56', '2021-10-23 15:58:56'),
(9, 1998, 'ADALAH BUKAN TAHUN KABISAT', '2021-11-03 00:33:52', '2021-11-03 00:33:52'),
(10, 2001, 'ADALAH BUKAN TAHUN KABISAT', '2021-11-03 00:34:00', '2021-11-03 00:34:00'),
(11, 2000, 'ADALAH TAHUN KABISAT', '2021-11-03 00:34:07', '2021-11-03 00:34:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `terbilang`
--

CREATE TABLE `terbilang` (
  `terbilang_id` int(11) NOT NULL,
  `code_barang` varchar(255) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `ket_terbilang` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `terbilang`
--

INSERT INTO `terbilang` (`terbilang_id`, `code_barang`, `nama_barang`, `qty`, `harga`, `total`, `ket_terbilang`, `created_at`, `updated_at`) VALUES
(13, 'NT2021910000001', 'Djarum Super', 10, 200000, 2000000, '', '2021-09-17 07:08:13', '0000-00-00 00:00:00'),
(14, 'NT2021910000002', 'Coklah Maluku kakka', 12, 1000, 12000, '', '2021-09-17 07:32:58', '0000-00-00 00:00:00'),
(15, 'NT2021910000003', 'Cumi Import', 10, 26000, 260000, '', '2021-09-17 07:14:26', '0000-00-00 00:00:00'),
(17, 'NT2021910000004', 'Djarum Coklat', 200, 2000000, 400000000, '', '2021-09-17 08:30:30', '0000-00-00 00:00:00'),
(19, 'NT2021910000006', 'Sepatu Nike Bola', 200, 2000000, 400000000, '', '2021-09-17 08:51:30', '0000-00-00 00:00:00'),
(20, 'NT2021910000007', 'Sajadah', 1000, 100000, 100000000, '', '2021-09-17 08:51:45', '0000-00-00 00:00:00'),
(21, 'NT2021910000008', 'Baju Koko', 200, 200000, 40000000, '', '2021-09-17 08:52:11', '0000-00-00 00:00:00'),
(22, 'NT2021910000009', 'Kaos Kaki', 1000, 50000, 50000000, '', '2021-09-17 08:52:39', '0000-00-00 00:00:00'),
(24, 'NT2021921911', 'Jersy Liverpool', 100, 20000, 2000000, '', '2021-09-17 12:23:24', '0000-00-00 00:00:00'),
(25, 'NT2021921921912', 'Super Mie', 100, 100000, 10000000, '', '2021-09-17 12:31:15', '0000-00-00 00:00:00'),
(26, 'NT2021921921913', 'Susu kambing', 13, 15300, 198900, '', '2021-09-17 15:13:37', '0000-00-00 00:00:00'),
(27, 'NT2021921921914', 'Djarum Super New', 12, 20000000, 240000000, '', '2021-09-21 03:05:25', '0000-00-00 00:00:00'),
(28, 'NT20211021921915', 'Coklat Belgia', 200, 2000000, 400000000, '', '2021-10-17 01:11:55', '0000-00-00 00:00:00'),
(29, 'NT20211021921916', 'Coklah Maluku', 10, 1000000, 10000000, '', '2021-10-23 03:25:07', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'pirlo', 'admin@ku.test', NULL, '$2y$10$jMMkWlIYaRHNR1FI3pdcq.wo6P5vbX2yDaXRhkxrV891CWnnTWiwa', NULL, '2020-11-08 07:04:13', '2020-11-08 07:04:13'),
(2, 'Amirin Syaifudin', 'amirinsyaifudin6@gmail.com', NULL, '$2y$10$0DWVvtyH1gR3cXRDCRSCzuDB.OD5FcF0w6wmiT1vTOQ1mD78TgwSO', NULL, '2020-11-14 07:14:44', '2020-11-14 07:14:44'),
(3, 'malik', 'malik@gmail.com', NULL, '$2y$10$PADYOTnhXuMxqUoBQvBzl.KOypOAvH8yRI5iQy2Y0iS.v795xF3em', NULL, '2020-11-14 07:15:07', '2020-11-14 07:15:07'),
(4, 'PARNO', 'parno@gmail.com', NULL, '$2y$10$K.ZNzEDKfw9MTjXBo2lzEOQerx5VO70nhnRbRAjmiYWvP4P8gMamG', NULL, '2021-11-26 05:48:56', '2021-11-26 05:48:56'),
(5, 'Admin Ku', 'pirlo@gmail.com', NULL, '$2y$10$zvbpWQBaqWjwB8zcGS0fD.bLE4eZrEV3EUaySNWyPQgphBexexxKS', NULL, '2022-02-04 22:40:51', '2022-02-04 22:40:51'),
(6, 'daroji', 'daroji@gmail.com', NULL, '$2y$10$H9sKGClLi0yn8oooftsPyuxrXlgegXZjJCsIVvomS/j1YxU8SfTLq', NULL, '2022-02-04 23:10:01', '2022-02-04 23:10:01'),
(7, 'ulya', 'ulya@gmail.com', NULL, '$2y$10$Bq.E8hVJhQtXwcrLG2KeLO3lbRyH/r2uAVjJqW1/M./vzBvKyrDhy', NULL, '2022-02-04 23:15:20', '2022-02-04 23:15:20'),
(8, 'riyan kcn', 'riyankcn@gmail.com', NULL, '$2y$10$mQYgG17684cpoKnkc5cNZu3xZvThvA17MXKr1u7CaYA7X5tAo2Mqu', NULL, '2022-03-24 08:46:21', '2022-03-24 08:46:21'),
(9, 'defri', 'defri@gmail.com', NULL, '$2y$10$tJC5y7WU6zr8odc.RC904.Aw7Xt5PyPe2x3x2hgqqdTXfnfCLXRKm', NULL, '2022-03-24 08:49:59', '2022-03-24 08:49:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `usia`
--

CREATE TABLE `usia` (
  `usia_id` int(11) NOT NULL,
  `usia` int(11) NOT NULL,
  `golongan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `usia`
--

INSERT INTO `usia` (`usia_id`, `usia`, `golongan`, `created_at`, `updated_at`) VALUES
(1, 15, 'ANAK KECIL ', '2021-10-24 05:38:12', '2021-10-24 05:38:12'),
(3, 12, '', '2021-10-24 05:40:44', '2021-10-24 05:40:44'),
(4, 28, 'DEWASA', '2021-10-24 05:47:41', '2021-10-24 05:47:41'),
(5, 60, 'ORANG TUA', '2021-10-24 05:47:50', '2021-10-24 05:47:50'),
(6, 28, 'DEWASA', '2021-11-03 01:11:16', '2021-11-03 01:11:16'),
(7, 12, 'ANAK ANAK', '2021-11-03 01:11:21', '2021-11-03 01:11:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `zodiak`
--

CREATE TABLE `zodiak` (
  `zodiak_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `zodiak`
--

INSERT INTO `zodiak` (`zodiak_id`, `nama`, `tanggal_lahir`, `keterangan`, `created_at`, `updated_at`) VALUES
(20, 'isco', '2021-03-03', 'Bulan Salah', '2021-10-29 02:35:11', '2021-10-29 02:35:11'),
(21, 'Ny. Suwarni', '2021-06-02', 'Bulan Salah', '2021-11-03 01:17:19', '2021-11-03 01:17:19'),
(22, 'Robby Pratama', '2021-11-04', 'Bulan Salah', '2021-11-10 13:53:17', '2021-11-10 13:53:17'),
(23, 'Robby Pratama', '2021-11-02', 'Bulan Salah', '2021-11-10 13:57:42', '2021-11-10 13:57:42'),
(24, 'Robby Pratama', '2021-11-02', 'tanggal salah', '2021-11-10 13:59:17', '2021-11-10 13:59:17'),
(25, 'Aisah Putri Pratiwi', '2021-11-04', 'tanggal salah', '2021-11-10 14:02:58', '2021-11-10 14:02:58'),
(26, 'Aisah Putri Pratiwi', '2021-11-01', 'tanggal salah', '2021-11-10 14:03:09', '2021-11-10 14:03:09'),
(27, 'Isti', '2021-11-03', 'tanggal salah', '2021-11-11 00:25:29', '2021-11-11 00:25:29'),
(28, 'Sahrukan India', '1996-02-13', 'tanggal salah', '2021-11-11 00:28:52', '2021-11-11 00:28:52'),
(29, 'kaka', '2021-11-03', 'tanggal salah', '2021-11-11 02:33:38', '2021-11-11 02:33:38');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `agama`
--
ALTER TABLE `agama`
  ADD PRIMARY KEY (`agama_id`);

--
-- Indeks untuk tabel `bis`
--
ALTER TABLE `bis`
  ADD PRIMARY KEY (`bis_id`);

--
-- Indeks untuk tabel `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indeks untuk tabel `cuti`
--
ALTER TABLE `cuti`
  ADD PRIMARY KEY (`cuti_id`);

--
-- Indeks untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `fungsi_terbilang`
--
ALTER TABLE `fungsi_terbilang`
  ADD PRIMARY KEY (`terbilang_id`);

--
-- Indeks untuk tabel `ganjilgenap`
--
ALTER TABLE `ganjilgenap`
  ADD PRIMARY KEY (`ganjilgenap_id`);

--
-- Indeks untuk tabel `golongan_darah`
--
ALTER TABLE `golongan_darah`
  ADD PRIMARY KEY (`golongan_darah_id`);

--
-- Indeks untuk tabel `gol_jam_kerja`
--
ALTER TABLE `gol_jam_kerja`
  ADD PRIMARY KEY (`gol_jam_kerja_id`);

--
-- Indeks untuk tabel `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`hari_id`);

--
-- Indeks untuk tabel `hitungbb`
--
ALTER TABLE `hitungbb`
  ADD PRIMARY KEY (`hitungbb_id`);

--
-- Indeks untuk tabel `hitungkeliling`
--
ALTER TABLE `hitungkeliling`
  ADD PRIMARY KEY (`hitungkeliling_id`);

--
-- Indeks untuk tabel `hitungluas`
--
ALTER TABLE `hitungluas`
  ADD PRIMARY KEY (`hitungluas_id`);

--
-- Indeks untuk tabel `hitungparkir`
--
ALTER TABLE `hitungparkir`
  ADD PRIMARY KEY (`hitungparkir_id`);

--
-- Indeks untuk tabel `jamkerja`
--
ALTER TABLE `jamkerja`
  ADD PRIMARY KEY (`jamkerja_id`);

--
-- Indeks untuk tabel `jamswitch`
--
ALTER TABLE `jamswitch`
  ADD PRIMARY KEY (`jamswitch_id`);

--
-- Indeks untuk tabel `jarakwaktu`
--
ALTER TABLE `jarakwaktu`
  ADD PRIMARY KEY (`jarakwaktu_id`);

--
-- Indeks untuk tabel `jenispasien`
--
ALTER TABLE `jenispasien`
  ADD PRIMARY KEY (`jenispasien_id`);

--
-- Indeks untuk tabel `jqform`
--
ALTER TABLE `jqform`
  ADD PRIMARY KEY (`jqform_id`);

--
-- Indeks untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`kabupaten_id`),
  ADD KEY `kabupaten_provinsi_id_foreign` (`provinsi_id`),
  ADD KEY `kabupaten_kota_id_foreign` (`kota_id`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`karyawan_id`),
  ADD KEY `cuti_id` (`cuti_id`);

--
-- Indeks untuk tabel `kategori_cuti`
--
ALTER TABLE `kategori_cuti`
  ADD PRIMARY KEY (`kategori_cuti_id`);

--
-- Indeks untuk tabel `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`kota_id`),
  ADD KEY `kota_provinsi_id_foreign` (`provinsi_id`);

--
-- Indeks untuk tabel `listkerjaan`
--
ALTER TABLE `listkerjaan`
  ADD PRIMARY KEY (`listkerjaan_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indeks untuk tabel `nilailulus`
--
ALTER TABLE `nilailulus`
  ADD PRIMARY KEY (`nilailulus_id`);

--
-- Indeks untuk tabel `nilaimhs`
--
ALTER TABLE `nilaimhs`
  ADD PRIMARY KEY (`nilaimhs_id`);

--
-- Indeks untuk tabel `parkir`
--
ALTER TABLE `parkir`
  ADD PRIMARY KEY (`parkir_id`);

--
-- Indeks untuk tabel `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`pasien_id`),
  ADD KEY `jenis_pasien_id` (`jenispasien_id`) USING BTREE,
  ADD KEY `poliklinik_id` (`poliklinik_id`),
  ADD KEY `agama_id` (`agama_id`),
  ADD KEY `golongan_darah_id` (`golongan_darah_id`),
  ADD KEY `pekerjaan_id` (`pekerjaan_id`),
  ADD KEY `pendidikan_id` (`pendidikan_id`),
  ADD KEY `status_perkawinan_id` (`status_perkawinan_id`),
  ADD KEY `suku_id` (`suku_id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`pekerjaan_id`);

--
-- Indeks untuk tabel `pembagian`
--
ALTER TABLE `pembagian`
  ADD PRIMARY KEY (`pembagian_id`);

--
-- Indeks untuk tabel `pembagian_js`
--
ALTER TABLE `pembagian_js`
  ADD PRIMARY KEY (`pembagianjs_id`);

--
-- Indeks untuk tabel `pendidikan`
--
ALTER TABLE `pendidikan`
  ADD PRIMARY KEY (`pendidikan_id`);

--
-- Indeks untuk tabel `pengurangan`
--
ALTER TABLE `pengurangan`
  ADD PRIMARY KEY (`pengurangan_id`);

--
-- Indeks untuk tabel `pengurangan_js`
--
ALTER TABLE `pengurangan_js`
  ADD PRIMARY KEY (`penguranganjs_id`);

--
-- Indeks untuk tabel `penjumlahan`
--
ALTER TABLE `penjumlahan`
  ADD PRIMARY KEY (`penjumlahan_id`);

--
-- Indeks untuk tabel `penjumlahan_js`
--
ALTER TABLE `penjumlahan_js`
  ADD PRIMARY KEY (`pejumlahanjs_id`);

--
-- Indeks untuk tabel `perkalian`
--
ALTER TABLE `perkalian`
  ADD PRIMARY KEY (`perkalian_id`);

--
-- Indeks untuk tabel `perkalian_js`
--
ALTER TABLE `perkalian_js`
  ADD PRIMARY KEY (`perkalianjs_id`);

--
-- Indeks untuk tabel `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`perusahaan_id`);

--
-- Indeks untuk tabel `poliklinik`
--
ALTER TABLE `poliklinik`
  ADD PRIMARY KEY (`poliklinik_id`);

--
-- Indeks untuk tabel `profil_pribadi`
--
ALTER TABLE `profil_pribadi`
  ADD PRIMARY KEY (`profil_pribadi_id`);

--
-- Indeks untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`provinsi_id`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indeks untuk tabel `status_perkawinan`
--
ALTER TABLE `status_perkawinan`
  ADD PRIMARY KEY (`status_perkawinan_id`);

--
-- Indeks untuk tabel `suku`
--
ALTER TABLE `suku`
  ADD PRIMARY KEY (`suku_id`);

--
-- Indeks untuk tabel `tahunkabisat`
--
ALTER TABLE `tahunkabisat`
  ADD PRIMARY KEY (`tahunkabisat_id`);

--
-- Indeks untuk tabel `terbilang`
--
ALTER TABLE `terbilang`
  ADD PRIMARY KEY (`terbilang_id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `usia`
--
ALTER TABLE `usia`
  ADD PRIMARY KEY (`usia_id`);

--
-- Indeks untuk tabel `zodiak`
--
ALTER TABLE `zodiak`
  ADD PRIMARY KEY (`zodiak_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `agama`
--
ALTER TABLE `agama`
  MODIFY `agama_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `bis`
--
ALTER TABLE `bis`
  MODIFY `bis_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `company`
--
ALTER TABLE `company`
  MODIFY `company_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `cuti`
--
ALTER TABLE `cuti`
  MODIFY `cuti_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT untuk tabel `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `fungsi_terbilang`
--
ALTER TABLE `fungsi_terbilang`
  MODIFY `terbilang_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `ganjilgenap`
--
ALTER TABLE `ganjilgenap`
  MODIFY `ganjilgenap_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `golongan_darah`
--
ALTER TABLE `golongan_darah`
  MODIFY `golongan_darah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `gol_jam_kerja`
--
ALTER TABLE `gol_jam_kerja`
  MODIFY `gol_jam_kerja_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `hari`
--
ALTER TABLE `hari`
  MODIFY `hari_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `hitungbb`
--
ALTER TABLE `hitungbb`
  MODIFY `hitungbb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `hitungkeliling`
--
ALTER TABLE `hitungkeliling`
  MODIFY `hitungkeliling_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `hitungluas`
--
ALTER TABLE `hitungluas`
  MODIFY `hitungluas_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `hitungparkir`
--
ALTER TABLE `hitungparkir`
  MODIFY `hitungparkir_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `jamkerja`
--
ALTER TABLE `jamkerja`
  MODIFY `jamkerja_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `jamswitch`
--
ALTER TABLE `jamswitch`
  MODIFY `jamswitch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `jarakwaktu`
--
ALTER TABLE `jarakwaktu`
  MODIFY `jarakwaktu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `jenispasien`
--
ALTER TABLE `jenispasien`
  MODIFY `jenispasien_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `kabupaten_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `karyawan_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT untuk tabel `kota`
--
ALTER TABLE `kota`
  MODIFY `kota_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `listkerjaan`
--
ALTER TABLE `listkerjaan`
  MODIFY `listkerjaan_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT untuk tabel `nilailulus`
--
ALTER TABLE `nilailulus`
  MODIFY `nilailulus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `nilaimhs`
--
ALTER TABLE `nilaimhs`
  MODIFY `nilaimhs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `parkir`
--
ALTER TABLE `parkir`
  MODIFY `parkir_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `pasien`
--
ALTER TABLE `pasien`
  MODIFY `pasien_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `pembagian`
--
ALTER TABLE `pembagian`
  MODIFY `pembagian_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `pendidikan`
--
ALTER TABLE `pendidikan`
  MODIFY `pendidikan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `pengurangan`
--
ALTER TABLE `pengurangan`
  MODIFY `pengurangan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `penjumlahan`
--
ALTER TABLE `penjumlahan`
  MODIFY `penjumlahan_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `perkalian`
--
ALTER TABLE `perkalian`
  MODIFY `perkalian_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `perusahaan_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `poliklinik`
--
ALTER TABLE `poliklinik`
  MODIFY `poliklinik_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `profil_pribadi`
--
ALTER TABLE `profil_pribadi`
  MODIFY `profil_pribadi_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `provinsi_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `status_perkawinan`
--
ALTER TABLE `status_perkawinan`
  MODIFY `status_perkawinan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `suku`
--
ALTER TABLE `suku`
  MODIFY `suku_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `tahunkabisat`
--
ALTER TABLE `tahunkabisat`
  MODIFY `tahunkabisat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `terbilang`
--
ALTER TABLE `terbilang`
  MODIFY `terbilang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `usia`
--
ALTER TABLE `usia`
  MODIFY `usia_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `zodiak`
--
ALTER TABLE `zodiak`
  MODIFY `zodiak_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD CONSTRAINT `kabupaten_kota_id_foreign` FOREIGN KEY (`kota_id`) REFERENCES `kota` (`kota_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `kabupaten_provinsi_id_foreign` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`provinsi_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kota`
--
ALTER TABLE `kota`
  ADD CONSTRAINT `kota_provinsi_id_foreign` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`provinsi_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
