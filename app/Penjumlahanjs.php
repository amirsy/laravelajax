<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjumlahanjs extends Model
{
    public $guarded = [];

    public $table   = 'penjumlahan_js';
}
