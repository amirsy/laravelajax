<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penguranganjs extends Model
{
    public $guarded = [];

    public $table   = 'pengurangan_js';
}
