<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perkalianjs extends Model
{
    public $guarded = [];

    public $table   = 'perkalian_js';
}
