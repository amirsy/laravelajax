<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Provinsi;
use App\Kota;
use DataTables;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // data-title-edit="' . $row->provinsi_id . '" 
        if ($request->ajax()) {
            $data = Kota::select(
                'kota.kota_id',
                'kota.nama_kota',
                'kota.kode_pos',
                'kota.keterangan',
                'provinsi.provinsi_id',
                'provinsi.nama_provinsi',
            )
                ->join('provinsi', 'kota.provinsi_id', '=', 'provinsi.provinsi_id');
            return datatables::of($data)
                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip" 
                    data-id="' . $row->kota_id . '" 
                    data-title="' . $row->nama_kota . '" 
                    data-kode_pos="' . $row->kode_pos . '" 
                    data-keterangan="' . $row->keterangan . '" 
                    data-original-title="Edit" 
                    class="edit btn btn-primary btn-sm editKota">EDIT </a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  
                    data-kota_id="' . $row->kota_id . '" 
                    data-original-title="Delete" 
                    class="btn btn-danger btn-sm deleteKota">DETELE</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->toJson();
            exit();
        }

        $provinsi = Provinsi::all();

        $kota = DB::table('kota')
            ->join('provinsi', 'kota.provinsi_id', '=', 'provinsi.provinsi_id')
            ->select(
                'kota.kota_id',
                'kota.nama_kota',
                'kota.kode_pos',
                'kota.keterangan',
                'provinsi.provinsi_id',
                'provinsi.nama_provinsi',
            )
            ->orderBy('kota.nama_kota', 'ASC')
            ->get();

        return view('admin.kota.index', compact('kota', 'provinsi'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Provinsi $provinsi)
    {
        $provinsi = Provinsi::all();

        $kota = new Kota;
        $kota->nama_kota      = $request->input('nama_kota');
        $kota->provinsi_id    = $request->input('provinsi_id');
        $kota->kode_pos       = $request->input('kode_pos');
        $kota->keterangan     = $request->input('keterangan');
        $kota->save();

        return response()->json(['success' => 'Kota Berhasil di tambahkan !!!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kota_id)
    {
        $kota = DB::table('kota')
            ->where('kota_id', $kota_id)
            ->first();

        return response()->json($kota);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('kota')
            ->where('kota_id', $request->id)
            ->update([
                'nama_kota'             => $request->nama_kota,
                'provinsi_id'           => $request->provinsi_id,
                'kode_pos'              => $request->kode_pos,
                'keterangan'            => $request->keterangan,
            ]);

        return response()->json(['success' => 'Kota berhasil di Update !!!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Kota::where('kota_id', $request->id)->delete();

        return response()->json(['success'  => 'Data Kota berhasil di hapus !!!']);
    }
}