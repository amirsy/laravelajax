<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Karyawan;
use App\Cuti;
use Illuminate\Http\Request;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $karyawan = Karyawan::all();
        $cuti     = Cuti::all();

        $company = DB::table('company')
            ->join('karyawan', 'company.karyawan_id', '=', 'karyawan.karyawan_id')
            ->join('cuti', 'company.cuti_id', '=', 'cuti.cuti_id')
            ->select(
                'company.company_id',
                'company.nama_company',
                'company.tanggal_gabung_karyawan',
                'company.lama_cuti_karyawan',
                'company.harga',
                'company.email',
                'company.cover',
                'company.keterangan',
                'company.alamat',
                'karyawan.karyawan_id',
                //   'karyawan.nama_karyawan',
                'cuti.cuti_id',
                // 'cuti.nama_cuti',
            )
            ->orderBy('company.nama_company', 'ASC')
            ->get();

        if ($request->ajax()) {
            $data = Company::latest()->get();
            return datatables::of($company)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip" 
                      
                        data-original-title="Edit" 
                        class="edit btn btn-primary btn-sm editCompany">EDIT </a>';

                    $btn = $btn . ' <a href="javascript:void(0)" data-toggle="tooltip"  
                        data-company_id="' . $row->company_id . '" 
                        data-original-title="Delete" 
                        class="btn btn-danger btn-sm deleteCompany">DETELE</a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.company.index', compact('karyawan', 'cuti', 'company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
