<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    //
    public $guarded = [];
    protected $primaryKey = 'kabupaten_id ';
    public $table   = 'kabupaten';


    //use HasFactory;
    // protected $table = 'tbl_kabupaten';
    // protected $fillable = [
    //     'kab_nama',
    //     'kab_prov'
    // ];
    // protected $primaryKey = 'kab_kode';


    // public $incrementing = false;
    // protected $keyType = 'string';
}
