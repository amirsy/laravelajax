<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listkerjaan extends Model
{
    //
    public $guarded = [];
    protected $primaryKey = 'kota_id ';
    public $table   = 'listkerjaan';
}
